@extends('master')

@section('title')
  {{ $title }}
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-8">
    
    <article>
    	
    	<form method="POST" action="{{ $base_url }}contact" action="contact">
        <input type="hidden" name="hash" value="{{ $hash }}" />
        <div class="form-group">
          <label>Votre nom</label>
          <input type="text" name="nom" class="form-control" />
        </div>

        <div class="form-group">
          <label>Votre email</label>
          <input type="text" name="email" class="form-control" />
        </div>

        <div class="form-group">
          <label>Votre message</label>
          <textarea  name="message" class="form-control"></textarea>
        </div>
        
        <p><input type="submit" class="btn btn-primary" value="Envoyer" />
      </form>
    </article>
    

  </div>
  <div class="col-md-4">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong></strong> </h1>
        
      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">

        <div class="row">

        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection

@section('scripts')

@endsection