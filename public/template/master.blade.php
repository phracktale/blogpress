<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title></title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png" />

        
        <link rel="stylesheet" href="{{ $assets_url }}css/bootstrap.min.css" />
        <link rel="stylesheet" href="{{ $assets_url }}css/minoral.css">
        <link rel="stylesheet" href="{{ $assets_url }}css/styles.css" />


    </head>
    <body>
      <div id="page" >
        <header class="main">
          <div id="header-image">
              <h1 class="text-center">
                <a class="sitename" href="#">
                  {{ $blog_name }} <br/>
                  <small>{{ $blog_slogan }}</small>
                </a>
              </h1>
          </div>
          <nav role="navigation">
            <div class="container">
              <a href="{{ $base_url }}">Accueil</a> | <a href="{{ $base_url }}jean_forteroche">Jean Forteroche</a>  | <a href="{{ $base_url }}contact">Contact</a>
            </div>
          </nav>
        </header>
        <main class="container">
          
              <h2>@yield('title')</h2>
               <!--[if lt IE 8]>
                  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
              <![endif]-->
              
                {!! $flash !!}
             
             @yield('content')

            <hr>
        </main>
        <footer class="main">
          <div class="container">
            <p>&copy; Agence A 2016</p> <a href="{{ $base_url_backend }}">Admin</a>
          </div>
        </footer>
      </div>

      <script src="{{ $base_url }}vendor/components/jquery/jquery.js"></script>
      <script src="{{ $assets_url }}js/bootstrap.min.js"></script>
      <script src="{{ $assets_url }}js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
      <script src="{{ $assets_url }}js/app.js"></script>
      <script>
        @yield('scripts')
      </script>
    </body>
</html>
