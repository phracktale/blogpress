@extends('master')

@section('title')
  Erreur 404
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-8">
    @foreach ($posts as $post)
    	@php
		    $date = date('d/m/Y', strtotime($post->T02_publication_d));
		  @endphp
        <article>
        	
        	<h2><a href="{{ $base_url }}{{ $post->T02_url_va }}">{{ $post->T02_title_va }}</a></h2>
        	Publié le {{ $date }} par {{ $post->user->T01_firstname_va }} {{ $post->user->T01_lastname_va }}
        	<p>
        	{{ $post->T02_abstract_va }}
        	</p>
        	<p class="text-right"><a class="btn btn-primary btn-sm margin-bottom-20" href="{{ $base_url }}{{ $post->T02_url_va }}">Lire la suite</a></p>
        </article>
    @endforeach

  </div>
  <div class="col-md-4">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong></strong> </h1>
        
      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">

        <div class="row">

        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection

@section('scripts')

@endsection