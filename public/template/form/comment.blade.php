
    <!-- tile -->
    <section class="tile">
      <!-- tile widget -->
      <!-- tile body -->
      <div class="tile-body nopadding">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Postez un commentaire</h3>
          </div>
          <div class="panel-body">
            <form method="post">
              <input type="hidden" name="T02_codeinterne_i" value="{{ $T02_codeinterne_i }}" />
              <input type="hidden" name="T03_parent_i" value="0" />
              <div class="row">
                <div class="form-group col-sm-12">
                  <label for="T03_title_va" class="control-label">Titre</label>
                  <input type="text" class="form-control" name="T03_title_va" id="T03_title_va" value="{{ $T02_title_va }}" />
                  
                </div>
                <div class="form-group  col-sm-6">
                  <label for="T03_name_va" class="control-label">Votre nom</label>
                  <input type="text" class="form-control" name="T03_name_va" id="T03_name_va" value="{{ $T03_name_va }}"  />
                 
                </div>
                <div class="form-group col-sm-6">
                  <label for="T03_email_va" class="control-label">Votre email</label>
                  <input type="text" class="form-control" name="T03_email_va" id="T03_email_va" value="{{ $T03_email_va }}"  />
                  
                </div>
                <div class="form-group no-bottom-margin col-sm-12">
                  <label for="T03_content_va" class="control-label">Votre commentaire</label>
                  <textarea class="form-control" name="T03_content_va" id="T03_content_va">{{ $T03_content_va }}</textarea>
                </div>
              </div>
              <p class="text-right"><input type="submit" class="btn btn-primary" value="Envoyer" /></p>
            </form>
          </div>
        </div>
      </div>
      <!-- /tile body -->
      
    </section>
    <!-- /tile -->


    <div class="modal fade" id="commentDialog" tabindex="-1" role="dialog" aria-labelledby="commentDialogLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 class="modal-title thin" id="commentDialogLabel">Envoyer un commentaire</h3>
          </div>
          <div class="modal-body">
            <form method="post">
              <input type="hidden" name="T02_codeinterne_i"  id="T02_codeinterne_i" />
              <input type="hidden" name="T03_parent_i" id="T03_parent_i" />
              <input type="hidden" name="response" id="response" />
              <div class="row">
                <div class="form-group col-sm-12">
                  <label for="T03_title_va" class="control-label">Titre</label>
                  <input type="text" class="form-control" name="T03_title_va" id="T03_title_va" value="{{ $T02_title_va }}" />
                  
                </div>
                <div class="form-group  col-sm-6">
                  <label for="T03_name_va" class="control-label">Votre nom</label>
                  <input type="text" class="form-control" name="T03_name_va" id="T03_name_va"  value="{{ $T03_name_va }}"/>
                 
                </div>
                <div class="form-group col-sm-6">
                  <label for="T03_email_va" class="control-label">Votre email</label>
                  <input type="text" class="form-control" name="T03_email_va" id="T03_email_va" value="{{ $T03_email_va }}"  />
                  
                </div>
                <div class="form-group no-bottom-margin col-sm-12">
                  <label for="T03_content_va" class="control-label">Votre commentaire</label>
                  <textarea class="form-control" name="T03_content_va" id="T03_content_va">{{ $T03_content_va }}</textarea>
                </div>
              </div>
              <p class="text-right"><input type="submit" class="btn btn-primary" value="Envoyer" /></p>
            </form>


          </div>
        </div>
      </div>
    </div> 

    