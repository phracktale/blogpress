@extends('master')

@section('title')
  Jean Forteroche
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-8">
    
    
    <article>
    	
    	<p>Après des études en Angleterre, au Canada et en Suisse, il décrit sa propre vie en trois périodes : play-boy à plein temps de 1938 à 1941 ; voleur professionnel de 1930 à 1948, puis, écrivain à partir de 1948.</p>

      <p>Il s'intéresse à l'écriture pendant qu'il purge une peine en prison. Il amorce une vraie carrière littéraire en 1955 par la publication de deux autobiographies : Occupation: Thief et Gentleman at Crime. Il se lance ensuite dans le roman noir et donne dix-huit titres qui mettent le plus souvent en scène des anti-héros, souvent des voleurs, broyés par l’engrenage du crime où ils se sont jetés par désespoir ou rêvant naïvement à un magot qu'ils obtiendraient sans trop d'efforts, ce qui permet à l'auteur de développer alors son lourd discours moralisateur sur les bienfaits de l'honnêteté. Dans son premier roman, Tu peux courir, publié en 1956, un escroc parvient à s’évader de prison, mais doit se prémunir à la fois contre la police et contre ses anciens complices afin de récupérer le fruit d'un cambriolage qu’il a soigneusement caché. Ce roman a été adapté au cinéma par Seth Holt en 1958 sous le titre Nowhere to Go (en).</p>

      <p>Jean Forteroche a également créé trois séries policières. La première, qui ne manque pas d’humour, raconte les exploits de Harry Chalice, le roi britannique de l’embrouille, et de Crying Eddie, un jeune dur-à-cuire qui répond au surnom d’Eddie le deuil. La deuxième série narre les aventures de l’ancien cambrioleur Macbeth Bain, notamment dans Triples crosses (1958). La troisième et la plus longue série est consacrée aux enquêtes du détective-inspecteur John Raven de Scotland Yard qui, après avoir démissionné de son poste dans Le Chasseur de rats (1977) à cause d’une fausse accusation de pot-de-vin, devient un détective privé sans licence ayant élu domicile sur un bateau.</p>
    </article>
    

  </div>
  <div class="col-md-4">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong></strong> </h1>
        
      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">

        <div class="row">

        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection

@section('scripts')

@endsection