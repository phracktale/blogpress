@extends('master')

@section('title')
  {{ $post->T02_title_va }}
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-12">
    <a href="{{ $base_url }}">Retour</a>
    @php
		$date = date('d/m/Y à h:i:s', strtotime($post->T02_publication_d));
		@endphp
    <article>
    	<small>Par {{ $post->User->T01_firstname_va }} {{ $post->User->T01_lasstname_va }}, le {{ $date }}</small>
      <hr class="separ" />
    	
    	
        @if ($post->T02_logo_va != "")
          <img class="logo fl" src="{{ $post->T02_logo_va }}" alt="{{ $post->T02_title_va }}" />
        @endif

        <p class="abstract">{{ $post->T02_abstract_va }}</p>
        {{ $post->T02_content_va }}
        

      <hr class="separ" />
      {!! $form_comments !!}
      <section id="comments">
        

        {!! $comments !!}
      </section>
    </article>
    

  </div>
  
  <!-- CONTENT ARTICLE -->
@endsection

@section('scripts')
  $(function(){
    $(".opencommentDialog").on("click", function () {
       var T02_codeinterne_i = $(this).data('post');
       var T03_parent_i = $(this).data('parent');
       var response = $(this).data('response');
       $("#commentDialog .modal-body").find("#T02_codeinterne_i").val( T02_codeinterne_i );
       $("#commentDialog .modal-body").find("#T03_parent_i").val( T03_parent_i );
       $("#commentDialog .modal-body").find("#response").val( response );
    });

    $('.reportBtn').on('click', function(event){
      event.preventDefault();
      $(this).attr('disabled', true);
      codeinterne = $(this).data('codeinterne');

      setCookie('report_' + codeinterne, true, 60);

      $.get('{{ $base_url }}comments/report/' + codeinterne, {'ajax': 'true'}, function(data){
        console.log(data);
      })
    })
  })
@endsection