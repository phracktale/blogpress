@extends('master')

@section('title')
  Bienvenue sur notre site
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-8">
    @foreach ($posts as $post)
    	@php
		    $date = date('d/m/Y', strtotime($post->T02_publication_d));
		  @endphp
        <article class="row">
        	@if ($post->T02_logo_va != "")
            <div class="col-sm-3">
              <img class="logo" src="{{ $post->T02_logo_va }}" alt="{{ $post->T02_title_va }}" />
            </div>
            <div class="col-sm-9">
          @else
            <div class="col-sm-12">
          @endif
            <h2><a href="{{ $base_url }}{{ $post->T02_url_va }}">{{ $post->T02_title_va }}</a></h2>
          	Publié le {{ $date }} par {{ $post->User->T01_firstname_va }} {{ $post->User->T01_lastname_va }}
          	<p>
          	{{ $post->T02_abstract_va }}
          	</p>
          	<p class="text-right"><a class="btn btn-primary btn-sm margin-bottom-20" href="{{ $base_url }}{{ $post->T02_url_va }}">Lire la suite</a></p>
          </div>
        </article>
    @endforeach
    @include('modeles.pagination', ['nb_page' => $nb_page, 'current_page' => $current_page])

  <div class="col-md-4">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong></strong> </h1>
        
      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">

        <div class="row">

        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection

@section('scripts')
  $(document).ready(function(){
    $('li.active a').on('click', function(event){
      event.preventDefault();
    })
  })
@endsection