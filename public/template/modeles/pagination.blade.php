<div class="text-right">
    <ul class="pagination pagination-lg">
      <li class="disabled"><a href="{{ $previous_url }}">«</a></li>
      @for ($i = 1; $i <= $nb_page; $i++)
        @php
          if($current_page == $i)
          {
            $class = "active";
            $href = "#";
          }
          else
          {
            $class="";
            $href = $page_url . "?p=" . $i;
          }
        @endphp
      <li class="{{ $class }}"><a href="{{ $href }}">{{ $i }}</a></li>
      @endfor
      <li><a href="{{ $next_url }}">»</a></li>
    </ul>
  </div>
</div>
