﻿<?php
  require 'vendor/autoload.php';
  require "includes/config.php";
  require "includes/init.php";
  
  
  use Philo\Blade\Blade;
  use App\Config;
  use App\Debug;
  use App\Utils;
  use App\UI;
  use App\Router;
  use App\Myerrorhandler;
  use App\Controllers\FrontendController;
 

  session_start();
  
  Myerrorhandler::getInstance();

  /* on instancie le controleur de l'application */
  $app = new FrontendController($config->conn);
  $app->setConfig($config);


  /* Définition des urls de base */
  $utils->setBaseUrl($config->base_url);
  $utils->setAssetsUrl($config->assets_url);

   
  /* Initialisation système de template blade */
  $views =  $app->config()->path . '/public/template';
  $cache =  $app->config()->path . '/cache';

  $blade = new Blade($views, $cache);

  $view_params = false;

  /* On initialise le router */
  $router = new Router($config->conn, $blade);
  
  /* routage */
  $query =  $_SERVER['QUERY_STRING'];
  $request =  $_SERVER['REQUEST_URI'];
  $method =  $_SERVER['REQUEST_METHOD'];
  $router->add($query, $request, $method);

  /* 
    - Chercher si url libre est dans la base 
    - Chercher si le paramètre 'url' correspond à un controleur existant
    - Chercher si le paramètre 'url' correspond à une vue existante 
  */

  $view_params = $router->run($app);

  if($view_params['redirect'])
  {
    $util = new Utils();
    $util->redirect($view_params['redirect']);
  }
 
  /* - Chercher si le paramètre 'url' correspond à une route définie dans le controleur Frontend */
  if(!$view_params['view'])
  {
   $view_params = $router->call($app, ['home'], 'home');
  }
  
  /* On récupère les variables de vue si elles existent */
  $bladeDatas = $view_params['datas'];


  /* Variables par défaut de la vue */
  $bladeDatas["base_url"] =  $config->base_url;
  $bladeDatas["base_url_backend"] = $config->base_url_backend;
  $bladeDatas["assets_url"] =  $config->assets_url;

  /* traitement des messages flash */
  
  $messages_flash =  $view_params['flash'];
  $flash = "";

  foreach ($messages_flash as $mFlash)
  {
    $flash .= UI::flash($mFlash['type'], $mFlash['title'], $mFlash['message'] );
  }

  $bladeDatas["flash"] =  $flash;

  /* Sortie vers navigaateur */
  if ($blade->view()->exists($view_params['view']) )
  {
    $view = $view_params['view'];
  }
  else
  {
    $view = "404";
  }
 
  echo $blade->view()->make($view)->with($bladeDatas)->render();