<?php

	/*
		class Config

	*/
	namespace App;
	use \PDO;
	use \App\Debug;		

	class Config
	{	
		private $db_host = 'localhost';
		private $db_name = null;
		private $db_user = null;
		private $db_password = null;

		public $base_url = null;
		public $base_url_backend = null;

		public $path ;

		public $conn;

		public function __construct($conf)
		{
			foreach ($conf as $name => $val)
			{
				Debug::log($name .":" . $val);
				$this->{$name} = $val;
			}
		}

		public function init_db()
		{
			$dsn = "mysql:dbname=" . $this->db_name . ";host=" . $this->db_host;
			try
			{
				$conn = new PDO($dsn, $this->db_user, $this->db_password);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn->exec("SET CHARACTER SET utf8");
				$this->conn = $conn;
				return true;
			}
			catch(PDOException $Exception)
			{
				Debug::log($Exception->getMessage());
			}
			catch (Exception $e)
		    {
		        Debug::log('Erreur SQL : ' . $e->getMessage());
		    }
			
			return false;
			
		}

		private function conn()
		{
			return $this->conn;
		}
	}