<?php

	/*
		Class Object
	*/

	namespace App;
	
	
	class Object
	{

		public function __construct(array $fields)
		{
			$this->hydrate($fields);
		}

		public function hydrate(array $fields)
		{
			foreach($fields as $key => $value){
				$method = 'set'. $key;
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}
	}