<?php

	/*
		Class Comment

	*/

	namespace App;
	
	class Comment extends Object
	{
		public $T03_codeinterne_i;
		public $T02_codeinterne_i;
		public $T03_parent_i;
		public $T03_created_d;
		public $T03_ip_va;
		public $T03_title_va;
		public $T03_content_va;
		public $T03_name_va;
		public $T03_email_va;
		public $T03_state_va;
		public $T03_report_i;

		public $Post;

		public function __construct( $fields)
		{
			$this->hydrate($fields);
		}

		public function hydrate($donnees)
		{
			if($donnees)
			{
				foreach ($donnees as $key => $value)
				{
				  $method = 'set'.ucfirst($key);
				  
				  if (method_exists($this, $method))
				  {
				    $this->$method($value);
				  }
				}
			}
		}

		public function T03_codeinterne_i()
		{
			return $this->T03_codeinterne_i;
		}

		public function setT03_codeinterne_i($val)
		{
			$this->T03_codeinterne_i = $val;
		}

		public function T02_codeinterne_i()
		{
			return $this->T02_codeinterne_i;
		}

		public function setT02_codeinterne_i($val)
		{
			$this->T02_codeinterne_i = $val;
		}

		public function T03_parent_i()
		{
			return $this->T03_parent_i;
		}

		public function setT03_parent_i($val)
		{
			$this->T03_parent_i = $val;
		}

		

		public function T03_created_d()
		{
			return $this->T03_created_d;
		}

		public function setT03_created_d($val)
		{
			$this->T03_created_d = $val;
		}

		public function T03_ip_va()
		{
			return $this->T03_ip_va;
		}

		public function setT03_ip_va($val)
		{
			$this->T03_ip_va = $val;
		}

		public function T03_title_va()
		{
			return $this->T03_title_va;
		}

		public function setT03_title_va($val)
		{
			$this->T03_title_va = $val;
		}

		public function T03_content_va()
		{
			return $this->T03_content_va;
		}

		public function setT03_content_va($val)
		{
			$this->T03_content_va = $val;
		}

		public function T03_name_va()
		{
			return $this->T03_name_va;
		}

		public function setT03_name_va($val)
		{
			$this->T03_name_va = $val;
		}

		public function T03_email_va()
		{
			return $this->T03_email_va;
		}

		public function setT03_email_va($val)
		{
			$this->T03_email_va = $val;
		}

		public function T03_state_va()
		{
			return $this->T03_state_va;
		}

		public function setT03_state_va($val)
		{
			$this->T03_state_va = $val;
		}

		public function T03_report_i()
		{
			return $this->T03_report_i;
		}

		public function setT03_report_i($val)
		{
			$this->T03_report_i = $val;
		}
		
		public function setPost(Post $post)
		{
			$this->Post = $post;
		}

		public function post(Post $post)
		{
			return $this->Post;
		}
	}