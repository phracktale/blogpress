<?php

	/*

		Class Post
	*/

	namespace App;
	

	class Post extends Object
	{

		public $T02_codeinterne_i;
		
		public $T01_codeinterne_i;

		public $T02_url_va;

		public $T02_logo_va;

		public $T02_title_va;

		public $T02_subtitle_va;

		public $T02_abstract_va;

		public $T02_content_va;

		public $T02_type_va;

		public $T02_status_va;

		public $T02_creation_d;

		public $T02_update_d;

		public $T02_publication_d;
		
		public $User;


		public function __construct( $fields)
		{
			$this->hydrate($fields);
		}

		public function hydrate($donnees)
		{
			if($donnees)
			{
				foreach ($donnees as $key => $value)
				{
				  $method = 'set'.ucfirst($key);
				  
				  if (method_exists($this, $method))
				  {
				    $this->$method($value);
				  }
				}
			}
		}


		public function T02_codeinterne_i()
		{
			return $this->T02_codeinterne_i;
		}

		public function setT02_codeinterne_i($val)
		{
			$this->T02_codeinterne_i = $val;
		}

		public function T01_codeinterne_i()
		{
			return $this->T01_codeinterne_i;
		}

		public function setT01_codeinterne_i($val)
		{
			$this->T01_codeinterne_i = $val;
		}

		public function T02_url_va()
		{
			return $this->T02_url_va;
		}

		public function setT02_url_va($val)
		{
			$this->T02_url_va = $val;
		}

		public function T02_logo_va()
		{
			return $this->T02_logo_va;
		}

		public function setT02_logo_va($val)
		{
			$this->T02_logo_va = $val;
		}

		public function T02_title_va()
		{
			return $this->T02_title_va;
		}

		public function setT02_title_va($val)
		{
			$this->T02_title_va = $val;
		}

		public function T02_subtitle_va()
		{
			return $this->T02_subtitle_va;
		}

		public function setT02_subtitle_va($val)
		{
			$this->T02_subtitle_va = $val;
		}

		public function T02_abstract_va()
		{
			return $this->T02_abstract_va;
		}

		public function setT02_abstract_va($val)
		{
			$this->T02_abstract_va = $val;
		}

		public function T02_content_va()
		{
			return $this->T02_content_va;
		}

		public function setT02_content_va($val)
		{
			$this->T02_content_va = $val;
		}

		public function T02_type_va()
		{
			return $this->T02_type_va;
		}

		public function setT02_type_va($val)
		{
			$this->T02_type_va = $val;
		}

		public function T02_status_va()
		{
			return $this->T02_status_va;
		}

		public function setT02_status_va($val)
		{
			$this->T02_status_va = $val;
		}

		public function T02_creation_d()
		{
			return $this->T02_creation_d;
		}

		public function setT02_creation_d($val)
		{
			$this->T02_creation_d = $val;
		}

		public function T02_update_d()
		{
			return $this->T02_update_d;
		}

		public function setT02_update_d($val)
		{
			$this->T02_update_d = $val;
		}

		public function T02_publication_d()
		{
			return $this->T02_publication_d;
		}

		public function setT02_publication_d($val)
		{
			$this->T02_publication_d = $val;
		}


		public function setUser(User $user)
		{
			$this->User = $user;
		}

		public function user()
		{
			return $this->User;
		}
	}