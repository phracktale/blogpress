<?php

	namespace App;

	class Myerrorhandler 
	{
	  private function __construct() 
	  {
		  ini_set('html_errors', '0');
		  set_error_handler(array(&$this, 'error'));
		  assert_options (ASSERT_WARNING, 0);
		  register_shutdown_function(array(&$this, 'shutdown'));
		}
	 
	  public static function getInstance() {
	    static $Instance = null;
	    if ($Instance === null) {
	      $Instance = new MyerrorHandler();
	    }
	    return $Instance;
	  }

	  public function error($level, $message, $file, $line, $ctx){
		  if (error_reporting() > 0 && $level == 1) {
		  	echo "<h1>Erreur fatale</h1>Vous avez fait quelque chose d'incroyable !!! Mais ... ça ne marche pas... désolé ...";
		    echo "<br/><br/>" . " " . $file . " " . $line . "<br/>" . $message;
		  }
		  return false;
		}

		function shutdown() {
		  $error = error_get_last();
		  if ($error != null) {
		    $this->error($error['type'], $error['message'], $error['file'], $error['line'], 0);
		    exit;
		  }
		}

	}