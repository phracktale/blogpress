<?php
/********************************************
 * TEMPLATE   Version 1.00                  *
 ********************************************
 * Copyright 2003 - 2011  Thierry Cazalet   *
 ********************************************
 ********************************************
 * class.template.php                       *
 ********************************************
 */
 namespace app;
 
/* 
 * @package template
 * @author Thierry Cazalet
 * @Company phracktale
 * @copyright 2003 - 2009 Thierry Cazalet
 *
 * La classe Page dispose de toutes les fonctions afin de g�rer le syst�me de template
 *	
 * @param $page  Sortie html de la page web
 * @parem $replaceCount Valeur des variables globales de la template
 */
 
class Page
{
   
   var $page;
   var $const;
   var $replaceConst;
    
	
	var $locale;
    
/**
 * R�cup�re les fichiers sur le serveur qui vont servir de template � l'affichage de l'application
 * 1�re m�thode
 * $templateDir existe, $templateFile existe et $HF existe
 * le template retourn� sera du type :
 * header + template + footer
 * 3 fichiers distincts seront utilis�, un header et un footer pour le look g�n�ral de la page
 * et 1 fichier template pour mettre en forme le contenu de la page 
 *
 * 2�me m�thode
 * $templateDir existe, $templateFile existe mais pas $HF
 * Le template retourn� d�pends uniquement d'un seul fichier de template sp�cifi� dans $templateFile
 *
 * 3�me m�thode (� supprimer)
 * M�me r�sultat que pour la 2�me m�thode mais $templateDir = $templateDir.$templateFile
 */
	function Page($templateDir, $templateFile, $HF) 
	{
		GLOBAL $datapath, $locale, $urlPublic, $urlPro, $dataUrl, $url, $const, $replaceConst;
		
		$const = array("/{URL}/i", "/{URLDATA}/i", "/{URLPUBLIC}/i", "/{URLPPRO}/i", "/{LOCALE}/i");
		$replaceConst = array($url, $dataUrl, $urlPublic, $urlPro, $locale);
		
		$this->locale = isset($this->locale)?$this->locale:"fr";
		$template=$templateDir.$templateFile;
        
		$header=$templateDir."header".$HF;
		$footer=$templateDir."footer".$HF;
		if (file_exists($template) and file_exists($header) and file_exists($footer))
		{
			$this->page = join('', file($header));
			$this->page .= join('',file($template));
			$this->page .= join('',file($footer));
		}
		else if (file_exists($template))
		{
			
			$this->page = join('', file($template));
		}
		elseif (file_exists("default/erreur404.html"))
		{			
            $this->page = join('', file("default/erreur404.html"));
		}
        else
        {
            $this->page = "Cette page n'existe pas : " .$template;
        }
	}

    /*
	 *  Remplacement des variables template
	 *  @param array $tmpVars
     *  @param array $replace
	 *  @return void
     */
	function replace_tags($tmpVars = array(), $replace = array()) 
	{
		GLOBAL $dataPath, $const, $replaceConst, $dataUrl;
        if(!isset($const))$const = array();
        if(!isset($replaceConst))$replaceConst = array();
		$tmpVars = array_merge($tmpVars, $const);
		$replace = array_merge($replace, $replaceConst);
		$dicoFile = $dataPath."traductions/".$this->locale.".inc"	;
        if (file_exists($dicoFile))
        {   
        
            $contenu_array = file($dicoFile);
            $num_line=sizeof($contenu_array);
            while ( list( $numero_ligne, $ligne ) = each( $contenu_array ) ) 
            {
                $var=explode("=", $ligne);
                $$var[0];
                $$var[0]=rtrim(str_replace("::","=",$var[1]));
            }
        }
        // Recherche des variables du dico dans les templates
        if( preg_match_all('/\{lib(.*?)}/m', $this->page, $tmpDico))
        {
            for($k=0; $k<=count($tmpDico[0]); $k++)
            {
                $varDico[$k]='/'.$tmpDico[0][$k]."/i";
                $tmp='lib'.$tmpDico[1][$k];
                $replaceDico[$k]=$$tmp;
            }
            
        }
        else
        {
            $varDico = array();
            $replaceDico = array();
        }
        
        
        $tmpVars= array_merge($tmpVars, $varDico);
		$replace = array_merge($replace, $replaceDico);
        
        
		$this->page = $this->language();
		
		
		/*
		$this->page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="#" onclick="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="btnFunc$1()"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/btnRight.gif" name="R$1"></td></tr></table></div></div><script language="javascript">function btnFunc$1(){$4}</script>', $this->page);
		*/
		

        // Biblioth�que d'objets template
        $this->page = preg_replace('`{TEMPLATE src="blockheader" title="([^"]*)" class="([^"]*)"}`', '<div class="block"><div class="header"><div class="titre">$1</div></div><div id="$2">', $this->page);
        $this->page = preg_replace('`{TEMPLATE src="blockheader" title="([^"]*)" class="([^"]*)" width="([^"]*)"}`', '<div class="block" style="width:$3"><div class="header"><div class="titre">$1</div></div><div id="$2">', $this->page);
        $this->page = preg_replace('`{TEMPLATE src="blockfooter"}`', '</div><div class="footer">&nbsp;</div></div>', $this->page);
        $this->page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="#" onclick="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="$4"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $this->page);
		$this->page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="([^"]*)" onclick="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="document.location=\'$4\';$5"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $this->page);
		$this->page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="document.location=\'$4\'"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $this->page);
		
		if(preg_match('/\$_SESSION\[(.*?)\]/m', $this->page, $sess))
		{
			$tempVar = $_SESSION[$sess[1]];
		}
		else
		{
			$tempVar = "";
		}
		
		$this->page = preg_replace('#\$_SESSION\[(.*?)\]#', $tempVar,  $this->page, 1);
		
		
		$this->page = preg_replace($tmpVars, $replace, $this->page);
	}
	
    /*
	 *  Implantation du langage de template
	 *  @return $this->page
     */
	function language()
	{
	
		// Traitement des conditions {if variable == "quelquechose"}affiche �a{/if}
		// la condition doit �tre sur une m�me ligne (trouver poiur le saut de ligne)
		if( preg_match_all('/\{if (.*?)}(.*?){\/if}/m', $this->page, $out))
		for ($k=0; $k<count($out)+1; $k++)
		{
			
				$condition = $out[1][$k];
				if(preg_match('/(.*?) == \"(.*?)\"/', $condition, $cond))
				{
					$var = $cond[1];
					
					if(preg_match('/session\[(.*?)\]\[(.*?)\]/', $var, $sess))
					{
					 $tempVar = $_SESSION[$sess[1]][$sess[2]];
					  
					}
					else if(preg_match('/session\[(.*?)\]/', $var, $sess))
					{
					  $tempVar = $_SESSION[$sess[1]];
					}
					else
					{
						// $$var doit �tre global.
						$tempVar = $$var;
						
					}
					
					$resultat = $cond[2];
					
					if($tempVar == $resultat)
					{
						$code = $out[2][$k];
					}
					else
					{
						$code = "";
					}
				}
				
				
				
				$this->page = preg_replace('#\{if (.*?)}(.*?){/if}#', $code,  $this->page, 1);

		}
		
							
		return $this->page;
	
	}
	
    /*
	 *  Sortie HTML
	 *  @return $this->page
     */
	function output() 
	{
		return $this->page;
	}
}


?>