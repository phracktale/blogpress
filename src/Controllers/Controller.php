<?php

	/*
		Class Controller
	*/
	
	namespace App\Controllers;
	
	use \App\Models\OptionsManager;
	
	class Controller
	{
		public $_conn;

		public $_config;
		

		public function __construct($conn)
		{
			$this->setDb($conn);

			$optionsManager = new OptionsManager($conn);

			$option = $optionsManager->getOption('blog_name');
			$this->_blog_name = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_slogan');
			$this->_blog_slogan = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_email');
			$this->_blog_email = $option->T04_value_va;
			$this->_view_params['datas'] = [
				'blog_name' => $this->_blog_name,
				'blog_slogan' => $this->_blog_slogan,
				'blog_email' => $this->_blog_email,
				'user' => $_SESSION['user']
			]; 
		}

		private function setDb($conn)
		{
			$this->_conn = $conn;
		}

		public function setConfig($config)
		{
			$this->_config = $config;
		}

		public function config()
		{
			return $this->_config;
		}

		public function getControllerName()
		{
			return "Controller";
		}
	}