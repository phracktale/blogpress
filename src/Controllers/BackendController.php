<?php

	/*
		Class AppController

	*/
	namespace App\Controllers;

	use \App\User;
	use \App\Utils;
	use \App\Config;
	use \App\Models\UserManager;
	use \App\Controllers\AppController;
	use \App\Controllers\ArticlesController;
	use \App\Controllers\MediasController;
	use \App\Controllers\UserController;
	use \App\Models\PostsManager;
	use \App\Models\CommentsManager;
	use \App\Models\OptionsManager;

	class BackendController extends AppController
	{
		protected $_logged = false;
		protected $_user = null;

		public function __construct($conn)
		{
			parent::__construct($conn);
			
		}

		public function processUrl($url)
		{
			return false;			
		}
		
		public function logged()
		{
			return $this->_logged;
		}

		public function setLogged($val)
		{
			$this->_logged = $val;
		}

		public function user()
		{
			return $this->_user;
		}

		public function setUser(User $val)
		{
			$this->_user = $user;
		}

		public function identification($view_params, $func=null, $params=null)
		{
			$utils = new Utils();
			$login = $utils->getParam('login', 'post');
            $password = $utils->getParam('password', 'post');
            
            $userManager = new UserManager($this->_conn);
            $user = $userManager->login($login, $password);
            if($user)
            {
             	$_SESSION['user'] = $user;
              	$utils->redirect('dashboard');
            }
            else
            {
              $_SESSION['flash'] = [[
                "type" => "notification-danger",
                "title" => "Problème d'identification",
                "message" => "Vous n'avez pas accès à cela"
              ]];
            }

            //$utils->redirect('accueil');
		}

		public function logout($view_params, $funcr=null, $params=null)
		{
			unset($_SESSION['user']);
            $utils = new Utils();
			$utils->redirect('login');
            exit;
		}

		public function dashboard($view_params, $func=null, $params=null)
		{
			if(!$this->logged()){Utils::redirect('login');}
			
			// Statistiques des articles
			$postsManager = new PostsManager($this->_conn);
			$posts = $postsManager->selectAll('post');

			$nb_posts = $postsManager->count('post');
			$plup = Utils::pluriel($nb_posts);
			$nb_posts_published = $postsManager->count('post', 'published');
			$plupp = Utils::pluriel($nb_posts_published);
			$nb_posts_draft = $postsManager->count('post', 'draft');
			$plub = Utils::pluriel($nb_posts_draft);


			// Liste des commentaires signalés
			$commentsManager = new CommentsManager($this->_conn);
			$options['filter'] = "reported";
			$comments = $commentsManager->selectAll($options);

			// Statistiques commentaires
			$nb_comments = $commentsManager->count();
			$pluc = Utils::pluriel($nb_comments);
			$nb_comments_active = $commentsManager->count('active');
			$pluca = Utils::pluriel($nb_comments_active);
			$nb_comments_inactive = $commentsManager->count('inactive');
			$pluci = Utils::pluriel($nb_comments_inactive);
			$nb_comments_reported = $commentsManager->count('reported');
			$plucr = Utils::pluriel($nb_comments_reported);

			// Paramètres de la vue
			$view_params['datas']['title'] = 'Tableau de bords';
			$view_params['datas']['comments'] =  $comments;
			$view_params['datas']['articles_stats'] = "Il y a en tout " . $nb_posts . " article$plup dans le blog<br />dont " . $nb_posts_published . " article$plupp publié$plup<br/>et " . $nb_posts_draft . " brouillon$plub";
			$view_params['datas']['commentaires_stats'] = "Il y a en tout " . $nb_comments . " commentaire$pluc<br />dont " . $nb_comments_active . " commentaire$pluca publié$pluca<br/>" . $nb_comments_inactive . " commentaire$pluci rejeté$pluci<br/>et " . $nb_comments_reported . " signalé$plucr par des utilisateurs";
			
			return $view_params;
		}

		public function liste_articles($view_params, $function=null, $params=null)
		{
			if(!$this->logged()){Utils::redirect('login');}
			$postsManager = new PostsManager($this->_conn);

			$func = $function['function'];
			$func_var = $function['var'];
			switch ($func)
			{
				default:
					$posts = $postsManager->selectAll('post');
					break;

				case "filtre":

					if($func_var == "tous" ){$posts = $postsManager->selectAll('post');}
					if($func_var == "publies" ){$posts = $postsManager->selectAll('post', 'published');}
					if($func_var == "brouillons" ){$posts = $postsManager->selectAll('post', 'draft');}
					break;
			}

			

			$post = $postsManager->count('post');
			$post_published = $postsManager->count('post', 'published');
			$post_draft = $postsManager->count('post', 'draft');

			$view_params['datas'] = [
					'posts' 		=> $posts,
					'nb_posts'		=> $post,
					'nb_published'	=> $post_published,
					'nb_draft'		=> $post_draft,
				];
			
			return $view_params;
		}

		public function editer_article($view_params, $func=null, $params=null)
		{
			if(!$this->logged()){Utils::redirect('login');}

			$func = $func['function'];
			$func_var = $func['var'];

			$utils = new Utils();
			if($utils->getParam('hash', 'post') == $_SESSION['hash'])
			{
				$articles = new ArticlesController($this->_conn);
				$articles->update($func_var);
				unset($_SESSION['hash']);
			}
			if(!isset($_SESSION['hash'])){$_SESSION['hash'] = md5(time());}
			
			
			
			$articles = new ArticlesController($this->_conn);
			$view_params = $articles->get($func_var);

			$view_params['view'] = "postEdit";
			$view_params['datas']['hash'] = $_SESSION['hash'];
			return $view_params;
		}

		public function liste_commentaires($view_params, $function=null, $params=null)
		{
			if(!$this->logged()){Utils::redirect('login');}

			$commentsManager = new CommentsManager($this->_conn);
			


			/* Mise en session des pages courantes */
			if(isset($_GET['page_1'])){$_SESSION['pagination_comments']['current_page'];} 

			$current_page = isset($_GET['page']) ? $_GET['page'] : $commentsManager->currentPage();
			$commentsManager->setCurrentPage($current_page);

			$func = $function['function'];
			$func_var = $function['var'];

			switch ($func)
			{
				default:
					$comments = $commentsManager->selectAll();
					break;

				case "filtre":
					if($func_var == "tous" ){$comments = $commentsManager->selectAll();}
					if($func_var == "publies" ){$comments = $commentsManager->selectAll(['filter' => 'active']);}
					if($func_var == "rejetes" ){$comments = $commentsManager->selectAll(['filter' => 'inactive']);}
					if($func_var == "signales" ){$comments = $commentsManager->selectAll(['filter' => 'reported']);}
					break;
			}

			$nb_comments = $commentsManager->count();
			$nb_comments_active = $commentsManager->count('active');
			$nb_comments_inactive = $commentsManager->count('inactive');
			$nb_comments_reported = $commentsManager->count('reported');
			
			$view_params['datas'] = [
					'comments' => $comments,
					'nb_comments' => $nb_comments,
					'nb_active' => $nb_comments_active,
					'nb_inactive' => $nb_comments_inactive,
					'nb_reported' => $nb_comments_reported,
			];
			
			return $view_params;
		}

		public function parametres($view_params, $func=null, $params=null){
			if(!$this->logged()){Utils::redirect('login');}

			$utils = new Utils();
			if($utils->getParam('hash', 'post') == $_SESSION['hash'])
			{
				$optionsManager = new optionsManager($this->_conn);
				$optionsManager->updateKey('blog_name', $utils->getParam('blog_name', 'post'));
				$optionsManager->updateKey('blog_slogan', $utils->getParam('blog_slogan', 'post'));
				$optionsManager->updateKey('blog_email', $utils->getParam('blog_email', 'post'));
				unset($_SESSION['hash']);
			}
			if(!isset($_SESSION['hash'])){$_SESSION['hash'] = md5(time());}
			
			$view_params['datas']['hash'] = $_SESSION['hash'];

			$optionsManager = new OptionsManager($this->_conn);

			$option = $optionsManager->getOption('blog_name');
			$view_params['datas']['blog_name'] = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_slogan');
			$view_params['datas']['blog_slogan'] = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_email');
			$view_params['datas']['blog_email'] = $option->T04_value_va;
			

			

			return $view_params;
		}

		public function profil($view_params, $func=null, $params=null){
			if(!$this->logged()){Utils::redirect('login');}

			$util = new Utils();

			$user = $_SESSION['user'];
			if($util->getParam('hash', 'post') == $_SESSION['hash'])
			{
				$userController = new userController($this->_conn);
				$view_params = $userController->update($util->getParam('T01_codeinterne_i', 'post'));
				$userManager = new UserManager($this->_conn);
				$user = $userManager->get($codeinterne);
				if($user){$_SESSION['user'] = $user;}
				unset($_SESSION['hash']);
			}


			if(!isset($_SESSION['hash'])){$_SESSION['hash'] = md5(time());}
			$view_params['datas']['hash'] = $_SESSION['hash'];


			return $view_params;
		}

		public function upload_logo_article($view_params, $func=null, $params=null)
		{
			$medias = new MediasController($this->config());

			$retour = $medias->upload($_FILES['file']);
			
			exit;
		}

		public function getControllerName()
		{
			return "BackendController";
		}
	}