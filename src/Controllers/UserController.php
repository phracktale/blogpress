<?php

	/*
		Class UserController
	*/
	
	namespace App\Controllers;

	use \App\Utils;
	use \App\Post;
	use \App\User;
	use \App\Models\UserManager;

	class UserController extends Controller
	{

		public function __construct($conn)
		{
			parent::__construct($conn);
			
		}

		
		public function get($codeinterne)
		{
			$userManager = new UserManager($this->_conn);
			$user = $userManager->get($codeinterne);

			if(!$user)
			{
				$vars = [
					'redirect' => 'index',
					'flash' => [[
							'type' 		=> 'notification-warning',
							'title' 	=> 'Erreur',
							'message' 	=> 	'Cet utilisateur n\'existe pas'
						]],
					'datas' => [ ]
				];
			}
			else
			{
				$vars = [
					'view' => 'profil',
					'datas' => $user
				];

			}
			return is_null($user) ? false : $vars;
		}

		

		public function update($codeinterne)
		{
			$util = new Utils();
			$userManager = new UserManager($this->_conn);

			/* Vérification des champs */

			$fields = [];
			$err = [];
			
			$codeinterne = $util->xss($codeinterne);
			$login = $util->getParam('T01_login_va', 'post');
			$password = md5($util->getParam('T01_password_va', 'post'));
			$password_verif = md5($util->getParam('password_verif', 'post'));
			$email = $util->getParam('T01_email_va', 'post');
			$firstname = $util->getParam('T01_firstname_va', 'post');
			$lastname = $util->getParam('T01_lastname_va', 'post');

			/* Traitement des erreurs */

			if($password != "")
			{
				if($password == $password_verif)
				{
					$fields['T01_password_va'] = $password;
				}
				else
				{
					$err['T01_password_va'][] = "Les 2 mots de passes ne sont pas identiques";
				}
			}
			else
			{
				/* Si le mot de passe est vide on récupère la valeur en base pour pouvoir la resauvegarder avec l'update */
				$user = $userManager->get($codeinterne);
				$password = $user->T01_password_va;
			}

			if($login == "")
			{
				$err['T01_login_va'][] = "L'identifiant ne peut pas être vide";
			}

			if($email == "")
			{
				$err['T01_email_va'][] = "L'email ne peut pas être vide";
			}

			
			if(count($err) > 0)
			{
				$message = "";
				foreach ($err as $key => $aError)
				{
					foreach ($aError as $error)
					{
						$message .= "<br/>" . $error;
					}
				}
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet article' . $message
				]];

				$vars = [
					'view' => 'profil',
					'flash' => $flash,
					'datas' => [
						'controller' 			=> $this->_controller,
						'T01_login_va' 			=> $login,
						'T01_email_va' 			=> $email,
						'T01_firstname_va' 		=> $firstname,
						'T01_lastname_va' 		=> $lastname
					]
				];

				return $vars;
			}
			
			

			$fields = array_merge($fields, [
				'T01_codeinterne_i' => $codeinterne,
				'T01_login_va' => $login,
				'T01_email_va' => $email,
				'T01_firstname_va' => $firstname,
				'T01_lastname_va' => $lastname
			]);
			
			$user = new User($fields);
			$userManager = new UserManager($this->_conn);
			$result = $userManager->update($user);
			if(!$result)
			{
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet utilisateur'
				]];
			}
			else
			{
 				$flash =  [[
					'type' 		=> 'notification-success',
					'title' 	=> 'Utilisateur mis à jour',
					'message' 	=> 	''
				]];
			}

			
			$vars = [
				'view' => 'profil',
				'flash' => $flash,
				'datas' => [
					'user' => $user
				]
			];

			return $vars;

		}

		public function create()
		{
			
		}

		public function save()
		{
			
		}

		public function getControllerName()
		{
			return "UserController";
		}

	}