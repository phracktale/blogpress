<?php

	/*
	 * Classe Medias
	 */

	namespace App\Controllers;

	
	class MediasController extends Controller
	{
		public $_mediasPath;

	 	public function __construct($config)
	 	{
	 		parent::__construct($config->conn);

	 		$this->_mediasPath = $config->path . "uploads/";
			
	 	}

	 	public function upload($file)
	 	{
	 		
			if (!empty($file)) 
			{
				if (!file_exists($this->_mediasPath)) 
				{
					if(!mkdir($this->_mediasPath, 0775, true))
					{
						/*
						header('HTTP/1.1 500 Internal Server Error');
						header('Content-type: text/plain');
						exit('Impossible de créer la structure de répertoire');
						*/
						$message = [
							'status' => 'error',
							'message' => 'Impossible de créer la structure de répertoire: ' .  $this->_mediasPath
						];
						echo json_encode($message);
						exit;
					}

				}
				
				
				$tempFile = $file['tmp_name'];  
				$type = $file['type'];
				$targetFile =  $this->_mediasPath . $file['name'];
				
				if(!move_uploaded_file($tempFile, $targetFile))
			    {
			    	/*
			    	header('HTTP/1.1 500 Internal Server Error');
					header('Content-type: text/plain');
					*/
					$message = [
						'status' => 'error',
						'message' => 'Impossible de sauver sur le serveur'
					];
					echo json_encode($message);
					exit;
			    }
			    else
			    {
			    	$message = [
						'status' => 'success',
						'targetFile' => $file['name']
					];
					echo json_encode($message);
			    }
			}
	 	}

	 	public function getControllerName()
		{
			return "MediasController";
		}

		public function thumbnail($img)
		{
			header('Content-type: text/html');
			return "OK";
			exit;
		}
	}