<?php

	/*
		Class AppController

	*/

	namespace App\Controllers;

	class AppController extends Controller
	{
		public $_url;
		public function __construct($conn)
		{
			parent::__construct($conn);
		}
		
		public function getControllerName()
		{
			return "AppController";
		}
	}