<?php

	/*
		Class CommentsController
	*/
	
	namespace App\Controllers;

	use \App\Models\PostsManager;
	use \App\Models\CommentsManager;
	use \App\Models\OptionsManager;
	use \App\utils;
	use \Swift_SmtpTransport,\Swift_Mailer,\Swift_Message;

	class CommentsController extends Controller
	{

		public function __construct($conn)
		{
			parent::__construct($conn);
			$optionsManager = new OptionsManager($this->_conn);

			
			
		}

		public function index()
		{
			
			
			return $vars;
		}



		public function display_comments($codeinterne, $options)
		{
			$comments_list = "";

			$commentsManager = new CommentsManager($this->_conn);
			if($options['filter'] == "reported")
			{
				$comments = $commentsManager->selectByPost($codeinterne);
			}
			else
			{
				$comments = $commentsManager->selectByPost($codeinterne);
			}
			
			
			ob_start();
			$this->format_comments($comments, $options);
			$comments_list = ob_get_contents();
			ob_end_clean();
			return $comments_list;
		}

		private function format_comments($comments, $options, $level=0)
		{
		    $prepend = str_repeat(' ', $level);

		    echo $prepend, '<ul>', PHP_EOL;
		    foreach($comments as $comment) {
		    	$date = date('d/m/Y à h:i:s', strtotime($comment['T03_created_d']));

		        echo $prepend . 
		        '    <li><b>' . 
		        htmlentities($comment['T03_title_va']) . "</b><br/>" . 
		        htmlentities($comment['T03_content_va']) .
		        '<small><br />par ' . $comment['T03_name_va'] . ' le ' . $date . '</small><hr class="separ" />';
		        PHP_EOL;
		        if($options['answer'])
		        {
		        	$disabled = "";
		        	if ($_COOKIE['report_' . $comment['T03_codeinterne_i']] == true)
		        	{
		        		$reportBtn = ' <span class="label label-warning">Vous avez signalé ce commentaire</span> ';
		        	}
		        	else
		        	{
		        		$reportBtn = '<a href="' . $this->config()->base_url . '/comments/report/' . $comment['T03_codeinterne_i'] . '" class="reportBtn btn btn-warning btn-xs" data-codeinterne="' . $comment['T03_codeinterne_i'] . '" title="Signaler un commentaire innaproprié" >Signaler</a>';
		        	}

		        	echo '<p class="text-right">' . $reportBtn . ' <a href="#commentDialog" role="button" data-toggle="modal" class="opencommentDialog btn btn-info btn-xs" data-post="' . $comment['T02_codeinterne_i'] . '" data-parent="' . $comment['T03_codeinterne_i'] . '" data-reponse="' . $comment['T03_name_va'] . '">Répondre</a></p>';
		        }

		        if ( !empty($comment['children']) ) {
		            $this->format_comments($comment['children'], $options, $level+1); // recurse into the next level
		        }
		        echo $prepend, '    </li>', PHP_EOL;
		    }
		    echo $prepend, '</ul>', PHP_EOL;
		}

		public function valider($codeinterne)
		{
			$commentsManager = new CommentsManager($this->_conn);
			$comment = $commentsManager->get($codeinterne);

			$comment->setT03_state_va('active');
			$comment->setT03_report_i(0);
			$commentsManager->update($comment);

			$vars =  [
				'redirect' => 'liste_commentaires'
			];
			
			return $vars;
		}

		public function rejeter($codeinterne)
		{
			$commentsManager = new CommentsManager($this->_conn);
			$comment = $commentsManager->get($codeinterne);

			$comment->setT03_state_va('inactive');
			$result = $commentsManager->update($comment);

			
			if(!$result)
			{
				$vars =  [
					'flash' => [[
						'type' 		=> 'notification-danger',
						'title' 	=> 'Erreur',
						'message' 	=> 	'Impossible de modérer ce commentaire'
					]]
				];
			}
			else
			{
				$vars =  [
					'redirect' => 'liste_commentaires',
					'flash' => [[
						'type' 		=> 'notification-succes',
						'title' 	=> 'Le commentaire est rejeté',
						'message' 	=> 	''
					]]
				];
			}
			
			
			return $vars;
		}

		public function supprimer($codeinterne)
		{
			$commentsManager = new CommentsManager($this->_conn);
			$comments = $commentsManager->delete($codeinterne);
			
			$vars =  [
				'redirect' 	=> 'liste_commentaires',
				'flash' 	=> [[
					'type' 		=> 'notification-succes',
					'title' 	=> 'Le commentaire est supprimé',
					'message' 	=> 	''
				]]
			];

			return $vars;
		}

		public function saveComment($cPost)
		{
			if($cPost)
			{
				$cPost['T03_created_d'] = Utils::now();
				$cPost['T03_ip_va'] = Utils::getUserIP();

				$commentManager = new CommentsManager($this->_conn);
			
				$comment = $commentManager->create($cPost, $type);

				if($comment)
				{
					return [
						"flash"	=> [[
							'type' 		=> 'notification-success',
							'title' 	=> 'Votre commentaire est envoyé'
						]]
					];
				}

				return [
						"flash"	=> [[
							'type' 		=> 'notification-danger',
							'title' 	=> 'Erreur',
							'message' 	=> 	'Impossible de sauvegarder le commentaire'
						]]
					];
			}
		}

		public function report($codeinterne)
		{
			if($_GET['ajax'] == "true")
			{
				$commentManager = new CommentsManager($this->_conn);
			
				$comment = $commentManager->get($codeinterne);
				$comment->T03_report_i += 1;
				$commentManager->update($comment);

				
				$util = new Utils();

				$sujet = 'Un commentaire a été signalé sur votre blog';
				$message = 'Un message a été signalé sur votre blog, merci de le réviser';

				$sent = $util->notifAdmin($sujet, $message, $this->_conn);

				if($sent)
				{
					$flash[] = [
		                "type" => "notification-success",
		                "title" => "Signalement effectué",
		                "message" => "Le signalement est effectué, nous le traiterons dès que possible."
		              ];
	             }
	             else
	             {
					$flash[] = [
		                "type" => "notification-danger",
		                "title" => "Problème de signalement",
		                "message" => "Le commentaire n'a pas été signalé car il y au un problème."
		              ];
	             }



				exit;
			}

			$vars =  [
				'redirect' => 'index'
			];


			return $vars;
		}

		public function getControllerName()
		{
			return "CommentsController";
		}

	}