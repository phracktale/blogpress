<?php

	/*
		Class PageController
	*/
	
	namespace App\Controllers;

	use \App\Models\PostsManager;
	use \App\utils;

	class PagesController extends PostsController
	{
		public $_type = "page";
		public $_title = "Pages";
		public $_controller = "pages";

		public function getControllerName()
		{
			return "PagesController";
		}
	}