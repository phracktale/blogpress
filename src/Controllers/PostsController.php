<?php

	/*
		Class PostsController
	*/
	
	namespace App\Controllers;

	use \App\Models\PostsManager;
	use \App\Models\CommentsManager;
	use \App\Models\CommentsController;
	use \App\Utils;
	use \App\Post;

	abstract class PostsController extends Controller
	{

		public function __construct($conn)
		{
			parent::__construct($conn);
			
		}

		public function index()
		{
			
			return $vars;
		}
		
		public function get($codeinterne)
		{
			$postsManager = new PostsManager($this->_conn);
			$post = $postsManager->get($codeinterne);
			$post = new Post($post);
			$post = $postsManager->setUser($post);

			if(!$post)
			{
				$vars = [
					'view' => 'posts',
					'redirect' => 'posts',
					'datas' => [ 
						'flash' => [[
							'type' 		=> 'notification-warning',
							'title' 	=> 'Erreur',
							'message' 	=> 	'Cet article n\'existe pas'
						]]
					]
				];
			}
			else
			{
				$statusChecked = $post->T02_status_va() == "published" ? "checked" : "";
				
				$vars = [
					'view' => 'postEdit',
					'datas' => [
						'statusChecked' 		=> $statusChecked,
						'controller' 			=> $this->_controller,
						'T02_codeinterne_i'		=> $post->T02_codeinterne_i,
						'T02_title_va'			=> $post->T02_title_va,
						'T02_type_va'			=> $this->_type,
						'T02_subtitle_va'		=> $post->T02_subtitle_va,
						'T02_logo_va' 			=> $post->T02_logo_va,
						'T02_url_va'			=> $post->T02_url_va,
						'T02_abstract_va'		=> $post->T02_abstract_va,
						'T02_content_va'		=> $post->T02_content_va
					]
				];

			}
			

			return is_null($post) ? false : $vars;
			
		}

		

		public function update($codeinterne)
		{
			$util = new Utils();

			$status = $util->getParam('T02_status_va', 'post');
			$status = $status == "on" ? "published" : "draft";
			
			/* Vérification des champs */
			
			/* Vérification des erreurs sur le formulaire */
			$err = [];
			/* Le titre n'est pas vide */
			if($util->getParam('T02_title_va', 'post') == "") {
				$err['T02_title_va'][] = 'Le titre ne peut pas être vide';
			}

			/* Préparation de l'url */
			$url = $util->getParam('T02_url_va', 'post') == "" ? $util->getParam('T02_title_va', 'post') : $util->getParam('T02_url_va', 'post');
			$url = $util->sanitizeStringForUrl($url, $this->_conn);

			/* L'url n'est pas vide */
			if($url == "") {
				$err['T02_url_va'][] = 'L\'url ne peut pas être vide';
			}


			if(count($err) > 0)
			{
				$message = "";
				foreach ($err as $key => $aError)
				{
					foreach ($aError as $error)
					{
						$message .= "<br/>" . $error;
					}
				}
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet article' . $message
				]];

				$vars = [
					'view' => 'postEdit',
					'flash' => $flash,
					'datas' => [
						'controller' 			=> $this->_controller,
						'T02_url_va' 			=> $url,
						'T02_logo_va' 			=> $util->getParam('T02_logo_va', 'post'),
						'T02_title_va' 			=> $util->getParam('T02_title_va', 'post'),
						'T02_subtitle_va' 		=> $util->getParam('T02_subtitle_va', 'post'),
						'T02_abstract_va' 		=> $util->getParam('T02_abstract_va', 'post'),
						'T02_content_va' 		=> $util->getParam('T02_content_va', 'post'),
						'statusChecked' 		=> $statusChecked
					]
				];

				return $vars;
			}


			$fields = [
				'T02_codeinterne_i' => $util->xss($codeinterne),
				'T02_url_va' => $url,
				'T02_logo_va' => $util->getParam('T02_logo_va', 'post'),
				'T02_title_va' => $util->getParam('T02_title_va', 'post'),
				'T02_subtitle_va' => $util->getParam('T02_subtitle_va', 'post'),
				'T02_abstract_va' => $util->getParam('T02_abstract_va', 'post'),
				'T02_content_va' => $util->getParam('T02_content_va', 'post'),
				'T02_status_va' => $status,
				'T02_update_d' => $util->now(),
				'T02_publication_d' => $util->now()
			];


			/* L'url doit être unique */
			$postsManager = new PostsManager($this->_conn);
			
			$posts = $postsManager->getByUrl($url);
			$extendUrl = false;
			if($posts)
			{
				$extendUrl = true;
			}

			$post = new Post($fields);
			
			
			if( $extendUrl)
			{
				$post->T02_url_va = $url . "-" . $post->T02_codeinterne_i;
			}

			$result = $postsManager->update($post);

			if(!$result)
			{
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet article'
				]];
			}
			else
			{
 				$flash =  [[
					'type' 		=> 'notification-success',
					'title' 	=> 'Article sauvegardé',
					'message' 	=> 	''
				]];
			}

			
			$statusChecked = $post->T02_status_va() == "published" ? "checked" : "";

			
			$vars = [
					'view' => 'postEdit',
					'flash' => $flash,
					'datas' => [
						'controller' 	=> $this->_controller,
						'T02_codeinterne_i'		=> $post->T02_codeinterne_i,
						'T02_title_va'			=> $post->T02_title_va,
						'T02_type_va'			=> $this->_type,
						'T02_subtitle_va'		=> $post->T02_subtitle_va,
						'T02_url_va'			=> $post->T02_url_va,
						'T02_abstract_va'		=> $post->T02_abstract_va,
						'T02_content_va'		=> $post->T02_content_va,
						'T02_logo_va'			=> $post->T02_logo_va,
						'statusChecked'			=> $statusChecked
						
					]
				];

			return $vars;

		}

		public function create()
		{
			$vars = [
					'view' => 'postCreate',
					'flash' => $flash,
					'datas' => [
						'controller' 	=> $this->_controller,
						
					]
				];

			return $vars;
		}

		public function save()
		{
			$util = new Utils();

			$status = $util->getParam('T02_status_va', 'post');
			$status = $status == "on" ? "published" : "draft";


			$postsManager = new PostsManager($this->_conn);

			/* Vérification des erreurs sur le formulaire */
			$err = [];
			/* Le titre n'est pas vide */
			if($util->getParam('T02_title_va', 'post') == "") {
				$err['T02_title_va'][] = 'Le titre ne peut pas être vide';
			}

			/* Préparation de l'url */
			$url = $util->getParam('T02_url_va', 'post') == "" ? $util->getParam('T02_title_va', 'post') : $util->getParam('T02_url_va', 'post');
			$url = $util->sanitizeStringForUrl($url, $this->_conn);

			/* L'url n'est pas vide */
			if($url == "") {
				$err['T02_url_va'][] = 'L\'url ne peut pas être vide';
			}
			

			
			if(count($err) > 0)
			{
				$message = "";
				foreach ($err as $key => $aError)
				{
					foreach ($aError as $error)
					{
						$message .= "<br/>" . $error;
					}
				}
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet article' . $message
				]];

				$vars = [
					'view' => 'postEdit',
					'flash' => $flash,
					'datas' => [
						'controller' 			=> $this->_controller,
						'T02_url_va' 			=> $url,
						'T02_logo_va' 			=> $util->getParam('T02_logo_va', 'post'),
						'T02_title_va' 			=> $util->getParam('T02_title_va', 'post'),
						'T02_subtitle_va' 		=> $util->getParam('T02_subtitle_va', 'post'),
						'T02_abstract_va' 		=> $util->getParam('T02_abstract_va', 'post'),
						'T02_content_va' 		=> $util->getParam('T02_content_va', 'post'),
						'statusChecked' 		=> $statusChecked
					]
				];

				return $vars;
			}


			
			$fields = [
				'T01_codeinterne_i' 	=> $_SESSION['user']->T01_codeinterne_i(),
				'T02_parent_i' 			=> 0,
				'T02_url_va' 			=> $url,
				'T02_logo_va' 			=> $util->getParam('T02_logo_va', 'post'),
				'T02_title_va' 			=> $util->getParam('T02_title_va', 'post'),
				'T02_subtitle_va' 		=> $util->getParam('T02_subtitle_va', 'post'),
				'T02_abstract_va' 		=> $util->getParam('T02_abstract_va', 'post'),
				'T02_content_va' 		=> $util->getParam('T02_content_va', 'post'),
				'T02_status_va' 		=> $status,
				'T02_type_va' 			=> $this->_type,
				'T02_creation_d' 		=> $util->now(),
				'T02_update_d' 			=> $util->now(),
				'T02_publication_d' 	=> $util->now()
			];

			
			
			/* L'url doit être unique */
			$posts = $postsManager->getByUrl($url);
			$extendUrl = false;
			if($posts)
			{
				$extendUrl = true;
			}

			$post = $postsManager->create($fields, $type);
			
			if( $extendUrl)
			{
				$post->T02_url_va = $url . "-" . $post->T02_codeinterne_i;
				$postsManager->update($post);
			}
			


			if(!$post)
			{
				$flash =  [[
					'type' 		=> 'notification-danger',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de sauvegarder cet article'
				]];
			}
			else
			{
 				$flash =  [[
					'type' 		=> 'notification-success',
					'title' 	=> 'Article sauvegardé'
				]];
			}

			$statusChecked = $post->T02_status_va() == "published" ? "checked" : "";

			$vars = [
					'view' => 'postEdit',
					'flash' => $flash,
					'datas' => [
						'controller' 	=> $this->_controller,
						'T02_codeinterne_i'		=> $post->T02_codeinterne_i,
						'T02_title_va'			=> $post->T02_title_va,
						'T02_type_va'			=> $this->_type,
						'T02_subtitle_va'		=> $post->T02_subtitle_va,
						'T02_url_va'			=> $post->T02_url_va,
						'T02_abstract_va'		=> $post->T02_abstract_va,
						'T02_content_va'		=> $post->T02_content_va,
						'T02_logo_va'			=> $post->T02_logo_va,
						'statusChecked' => $statusChecked
					]
				];

			return $vars;
		}

		public function delete($codeinterne)
		{
			$commentsManager = new CommentsManager($this->_conn);
			$result = $commentsManager->deleteFromPost($codeinterne);

			$postsManager = new PostsManager($this->_conn);
			$result = $postsManager->delete($codeinterne);
			if(!$result)
			
			{
				$flash =  [[
					'type' 		=> 'notification-error',
					'title' 	=> 'Erreur',
					'message' 	=> 	'Impossible de supprimer cet article' . $message
				]];
			}
			else
			{
				$flash =  [[
					'type' 		=> 'notification-success',
					'title' 	=> 'Suppression article',
					'message' 	=> 	'L\'article est supprimé'
				]];
			}

			
			$vars = [
					'redirect' => 'liste_articles',
					'flash' => $flash,
					'datas' => [
						'controller' 			=> $this->_controller
					]
				];


			return $vars;

		}

		public function getControllerName()
		{
			return "PostsController";
		}

	}