<?php

	/*
		Class ArticlesController
	*/
	
	namespace App\Controllers;

	use \App\Models\PostsManager;
	use \App\utils;

	class ArticlesController extends PostsController
	{
		public $_type = "post";
		public $_title = "Articles";
		public $_controller = "articles";

		public function getControllerName()
		{
			return "ArticlesController";
		}
	}