<?php

	/*
		Class PostsController
	*/
	
	namespace App\Controllers;

	use \App\Models\PostsManager;
	use \App\utils;
	use \App\Controllers\AppController;
	use \App\Controllers\CommentsController;
  	use Philo\Blade\Blade;


	class FrontendController extends AppController
	{
		
		public function __construct($conn)
		{
			parent::__construct($conn);
		}

		public function processUrl($url)
		{
			$this->_url = $url;

			/* Traitement des posts */
			$view_params =  $this->processPost();

			if($view_params){
				$this->_view_params['view'] = $view_params['view'];
				$this->_view_params['redirect'] = $view_params['redirect'];
				$this->_view_params['datas'] = array_merge($this->_view_params['datas'], $view_params['datas']);
				
				return $this->_view_params;
			}

			/* Autre traitement */

			return false;
		}

		/*
		 * Gère l'affichage d'un post
		 *
		 */

		private function processPost()
		{
			$postsManager = new PostsManager($this->_conn);
			$post = $postsManager->getByUrl($this->_url);
			
			if($post)
			{
				$commentsController = new CommentsController($this->_conn);

				$result = $commentsController->saveComment($_POST);
				$flash = $result['flash'];


				$comments = $commentsController->display_comments(
					$post->T02_codeinterne_i,
				 	[
				 		"answer" => true
				 	]
				);

				$datas["title"] =  $post->T02_title_va;

				/* Initialisation système de template blade */
				$views = $this->config()->path . 'public/template/form';
				$cache = $this->config()->path . 'cache';
				
				
				$datas = $_POST;
				$datas ["T02_codeinterne_i"] = $post->T02_codeinterne_i;
				$datas ["T02_title_va"] = $post->T02_title_va;
				$datas ["T03_parent_i"] = 0;


				$blade = new Blade($views, $cache);
  				$form_comments = $blade->view()->make('comment')->with($datas)->render();

				$view_params["datas"] = [
					"post" => $post,
					"form_comments" => $form_comments,
					"comments" => $comments
				];
				$view_params["flash"] = $flash;

				/* Passage des varialbes à la vue */
				$this->_view_params['view'] = $post->T02_type_va;
				$this->_view_params['redirect'] = $view_params['redirect'];

				$this->_view_params['datas'] = array_merge($this->_view_params['datas'], $view_params["datas"]);
				return $this->_view_params;

			}

			return false;
		}

		public function getPage()
		{

			
				return $this->_view_params;
		}


		public function home()
		{
			$util = new Utils();

			$p = $util->getParam('p', 'get');

			if($p == "")
			{
				$current_page = 1;
			}
			else
			{
				$current_page = $p;
			}

			$postsManager = new PostsManager($this->_conn);
			$nb_posts = $postsManager->count("post", "published");
			$nb_page = ceil($nb_posts / $this->config()->pagination);

			$postsManager->setPagination($this->config()->pagination);
			$postsManager->setCurrentPage($current_page);


			$posts = $postsManager->selectAll("post", "published");

			/* Passage des variables à la vue */
			$datas = [
				'posts' 		=> $posts,
				'current_page'	=> $current_page,
				'nb_page'		=> $nb_page
			];

			$this->_view_params['view'] = 'home';
			$this->_view_params['datas'] = array_merge($this->_view_params['datas'], $datas);
			//print_r($this->_view_params['datas']);
			return $this->_view_params;
		}

		
		public function contact()
		{

			$util = new Utils();
			
			
			if($util->getParam('hash', 'post') == $_SESSION['hash'] && isset($_SESSION['hash']))
			{
				$nom = $util->getParam('name', 'post');
				$email = $util->getParam('email', 'post', 'email');
				$message = $util->getParam('message', 'post');

				if(!$email) {
					$err['email'][] = "L'email est obligatoire";
				}

				if(!$message)
				{
					$err['message'][] = "Votre message est vide";
				}

				if(count($err) > 0)
				{
					$message = "";
					foreach ($err as $key => $aError)
					{
						foreach ($aError as $error)
						{
							$message .= "<br/>" . $error;
						}
					}

					$flash[] = [
			                "type" => "notification-danger",
			                "title" => "Problème dans le formulaire",
			                "message" => $message
			              ];
				}
				else
				{
					$sujet = 'Un visiteur de votre blog vous a envoyé un message';
					$message = 'Bonjour,\n\nUn email vous a été envoyé par: ' . $nom . ' <' . $email . '>\n\nVoici son message:\n\n' . $message;

					$sent = $util->notifAdmin($sujet, $message, $this->_conn);

					if($sent)
					{
						$flash[] = [
			                "type" => "notification-success",
			                "title" => "Envoi du message",
			                "message" => "Votre message a bien été envoyé, nous le traiterons dès que possible."
			              ];
		             }
		             else
		             {
						$flash[] = [
			                "type" => "notification-danger",
			                "title" => "Problème d'envoi du message",
			                "message" => "Votre email n'a pas été envoyé, il y a eu un problème."
			              ];
		             }
					unset($_SESSION['hash']);
				}
			}


			if(!isset($_SESSION['hash'])){$_SESSION['hash'] = md5(time());}

			/* Passage des variables à la vue */
			$this->_view_params['view'] = 'contact';

			$this->_view_params['redirect'] = $view_params['redirect'];

			$this->_view_params['datas']['hash'] = $_SESSION['hash'];
			$this->_view_params['flash'] = $flash;
			$this->_view_params['datas']['title'] = 'Contactez-nous';
			return $this->_view_params;
		}


		public function getControllerName()
		{
			return "FrontendController";
		}
	}