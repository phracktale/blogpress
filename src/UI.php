<?php

	/*
		Class UI



	*/

	namespace App;
	
	class UI
	{
		/*
		 * Renvoi une notification dans l'interface utilisateur
		 * @param $type  	classe css de la notification [notification-warning|notification-danger|notification-success|notification-info]
		 * @param $title  	titre de la notification
		 * @param $message 	Message de la notification
		 *
		 */

		static function flash($type = "notification-info", $title = "Attention!", $message = "")
		{
			return '<div class="notification no-top-margin ' . $type . '"><h4>' . $title . '</h4><p>' . $message . '</p></div>';
		}
	}