<?php

	/*
		Class User

	*/

	namespace App;
	
	class User extends Object
	{

		public $T01_codeinterne_i;
		public $T01_login_va;
		public $T01_email_va;
		public $T01_password_va;
		public $T01_firstname_va;
		public $T01_lastname_va;
		public $T01_created_d;
		public $T01_lastlog_d;


		public function __construct($user)
		{
			$this->hydrate($user);
		}

		public function hydrate($donnees)
		{
			if($donnees)
			{
				foreach ($donnees as $key => $value)
				{
				  $method = 'set'.ucfirst($key);
				  
				  if (method_exists($this, $method))
				  {
				    $this->$method($value);
				  }
				}
			}
		}
		

		public function T01_codeinterne_i()
		{
			return $this->T01_codeinterne_i;
		}

		public function setT01_codeinterne_i($val)
		{
			$this->T01_codeinterne_i = $val;
		}

		public function T01_email_va()
		{
			return $this->T01_email_va;
		}

		public function setT01_email_va($val)
		{
			$this->T01_email_va = $val;
		}

		public function T01_login_va()
		{
			return $this->T01_login_va;
		}

		public function setT01_login_va($val)
		{
			$this->T01_login_va = $val;
		}

		public function T01_password_va()
		{
			return $this->T01_password_va;
		}

		public function setT01_password_va($val)
		{
			$this->T01_password_va = $val;
		}

		public function T01_firstname_va()
		{
			return $this->T01_firstname_va;
		}

		public function setT01_firstname_va($val)
		{
			$this->T01_firstname_va = $val;
		}

		public function T01_lastname_va()
		{
			return $this->T01_lastname_va;
		}

		public function setT01_lastname_va($val)
		{
			$this->T01_lastname_va = $val;
		}

		public function T01_created_d()
		{
			return $this->T01_created_d;
		}

		public function setT01_created_d($val)
		{
			$this->T01_created_d = $val;
		}

		public function T01_lastlog_d()
		{
			return $this->T01_lastlog_d;
		}

		public function setT01_lastlog_d($val)
		{
			$this->T01_lastlog_d = $val;
		}

	}