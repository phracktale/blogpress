<?php

	/*
		Class Options

	*/

	namespace App;
	
	class Options extends Object
	{
		public $T04_codeinterne_i;
		public $T04_key_va;
		public $T04_value_va;

		public function __construct( $fields)
		{
			$this->hydrate($fields);
		}

		public function hydrate($donnees)
		{
			if($donnees)
			{
				foreach ($donnees as $key => $value)
				{
				  $method = 'set'.ucfirst($key);
				  
				  if (method_exists($this, $method))
				  {
				    $this->$method($value);
				  }
				}
			}
		}

		public function T04_codeinterne_i()
		{
			return $this->T04_codeinterne_i;
		}

		public function setT04_codeinterne_i($val)
		{
			$this->T03_codeinterne_i = $val;
		}

		public function T04_key_va()
		{
			return $this->T04_key_va;
		}

		public function setT04_key_va($val)
		{
			$this->T04_key_va = $val;
		}

		public function T04_value_va()
		{
			return $this->T04_value_va;
		}

		public function setT04_value_va($val)
		{
			$this->T04_value_va = $val;
		}

	}