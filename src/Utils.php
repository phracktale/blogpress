<?php

  namespace App;

  use \DateTimeZone;
  use \DateTime;
  use \App\Config;
  use \App\Models\OptionsManager;
  use \WideImage\WideImage;
  use \Swift_SmtpTransport,\Swift_Mailer,\Swift_Message;


  class Utils {
    public $_base_url;

    public $_assets_url;

    public $_conn;

    public $_filtre = "string";
    
    public function setBaseUrl($url){
      $this->_base_url =  $url;
    }

    public function setAssetsUrl($url){
      $this->_assets_url =  $url;
    }


    public function redirect($url)
    {
      header('location:' . $this->_base_url . $url);
      exit;
    }

    public function getParam($val, $method="get", $filtre="string")
    {
      
      $this->_filtre = $filtre;

      switch($method)
      {
        case "get":
          $val = (string) $_GET[$val];
          break;

        case "post":
          $val = (string) $_POST[$val];
          break;
      }
      
      return $this->xss($val);
    }


    public function xss($string)
    {
      /* 
      En cas d'inclusion à base de données
      return filter($string);
      */
      return $this->filter($string);
    }

    private function filter($value)
    {
      switch($this->_filtre)
      {
        default:
        case "string":// On regarde si le type de string est un nombre entier (int)
          if(ctype_digit($value))
          {
            $value = intval($value);
          }
          // Pour tous les autres types
          else
          {
            $value = filter_var($value, FILTER_SANITIZE_STRING);
          }
          break;

        case "email":
          $value = filter_var($value, FILTER_SANITIZE_EMAIL);
          $value = filter_var($value, FILTER_VALIDATE_EMAIL);
          break;
      }
      return $value;
    }

    public function mysql2frDate($date, $type_date="date")
    {
      if(!empty($date))
      {
        $_D = explode(' ', $date);
        $_T = $_D[1];
        $_D = explode('-', $_D[0]);
        switch($type_date)
        {
            default;
            case "date":
                return $_D[2]."/".$_D[1].'/'.$_D[0];
                break;
                
            case "datetime":
                return $_D[2]."/".$_D[1].'/'.$_D[0] . " " . $_T;
                break;

            case "time":
                return $_T;
                break;
        }
      }
    }
    public function now()
    {
      $DTZ = new DateTimeZone('Europe/Paris');
      $d = new DateTime();
      $d->setTimezone($DTZ);
      return $d->format('Y-m-d H:i:s');
    }
    public function fr2mysqlDate($date, $type_date="date")
    {
      if(!empty($date))
        {
            $_D = explode(' ', $date);
            
            $_T = isset($_D[1]) ? $_D[1] : "";
            $_D = explode('/', $_D[0]);
            
            
            switch($type_date)
            {
                default;
                case "date":
                    return $_D[2]."-".$_D[1].'-'.$_D[0];
                    break;
                    
                case "datetime":
                    $_T = $_T == "" ? "00:00:00" : $_T;
                    return $_D[2]."-".$_D[1].'-'.$_D[0] . " " . $_T;
                    break;

                case "time":
                    $_T = $_T == "" ? "00:00:00" : $_T;
                    return $_T;
                    break;
            }
        }
        else
        {
            return "0000-00-00";
        }
      }

      function sanitizeStringForUrl($string, $conn){
          $string = strtolower($string);
          $string = htmlentities($string, ENT_NOQUOTES, "UTF-8");
          $string = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $string);
          $string = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $string); // pour les ligatures e.g. '&oelig;'
          $string = preg_replace('#&[^;]+;#', '', $string); // supprime les autres caractères
          $string = preg_replace('#[\s]{2,}#',' ',$string);
          $string = str_replace(array(' '),array('-'),$string);
          return $string;
      }

      public function getUserIP()
      {
        return $_SERVER['REMOTE_ADDR'];
      }

      public function thumbnail($img, $options)
      {
        

        $imageR = WideImage::load($img)->resize($options['width']*2, $options['height']*2)->crop($options['width'], $options['height'], 'center', 'center');

        $infos = explode('.', $img);
        $extension = strtolower($img[1]);

        header('Content-type: image/' . $extension);
        echo $imageR;
      }

      public function notifAdmin($sujet, $message, $conn)
      {
        require "includes/config.php";

        $optionsManager = new OptionsManager($conn);

        $option = $optionsManager->getOption('blog_email');
        $email_admin = $option->T04_value_va;

        /* Envoi de notification à l'administrateur */
        $transport = \Swift_MailTransport::newInstance();
        $mailer = \Swift_Mailer::newInstance($transport);

        // Create a message
        $email = \Swift_Message::newInstance($sujet)
          ->setFrom(array($config_vars['sender']['email'] => $config_vars['sender']['name']))
          ->setTo(array($email_admin))
          ->setBody($message);

          return $mailer->send($email);
      }

      public function pagination($nb_page, $current_page)
      {
        
      }

      public function pluriel($nb)
      {
        return $nb>1?"s":"";
      }
  }
