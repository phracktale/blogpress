<?php
	/*
		Class Router
	*/
	namespace App;
	
	use \App\Models\PostsManager;
	use \App\Models\OptionsManager;

	class Router
	{
		protected $_conn;

		protected $_url;

		protected $_func;

		protected $_params;

		protected $_app;

		protected $_method;

		protected $_blade;

		protected $_blog_datas;


		public function __construct($conn, $blade)
		{
			$this->setConn($conn);
			$this->setBlade($blade);

			$optionsManager = new OptionsManager($conn);

			$option = $optionsManager->getOption('blog_name');
			$blog_name = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_slogan');
			$blog_slogan = $option->T04_value_va;

			$option = $optionsManager->getOption('blog_email');
			$blog_email = $option->T04_value_va;

			$this->_blog_datas = [
				'blog_name' 	=> $blog_name,
				'blog_slogan' 	=> $blog_slogan,
				'blog_email' 	=> $blog_email,
			]; 
		}

		public function add($query, $request, $method)
		{
			$query = explode('=', $query);
			$query = $query[1];
			$query = explode('/', $query);
			$this->setUrl($query[0]);



			$func = isset($query[1]) ? $query[1] : "index";
			$func_var = $query[2];
			$_FUNC = ['function' => $func, 'var' => $func_var];
			
			$this->setFunc($_FUNC);


			$request = explode('?', $request);
			$this->_params = $params;
			$this->setMethod($method);
			return true;
		}

		/*
 		 *  
		 *	@param $controller 		le controleur de l'application
		 *	@param $methods 		Tableau de chaînes, chaque éléments du tableau est obligatoirement une méthode existante du contrôleur
		 *	@param $default_view	Route par défaut
 		 *
		 */

		public function call($controller, $methods, $default_view="")
		{
			foreach($methods as $method)
			{
				if(($this->_url == $method || $default_view == $method) && method_exists($controller, $method))
				{
					return $controller->$method($vars);
				}
			}
			
			return false;
		}

		/*
 		 *  
		 *	@param $controller 		le controleur de l'application
		 *  @param $default_view	Route par défaut
		 */

		public function run($app, $default_view = "")
		{
			/* Initialisation */
			$view_params = false;
			$this->_url = $this->_url == "" ? $default_view : $this->_url;
			
			//echo $this->_url;
			//echo "<br/>" . $app->info() . " " . $method;

			/* 
				Methode 1
				Rechercher si l'url fournie correspond à une simple vue ou si elle  est une methode du controleur d'application.
			*/

			if ($this->blade()->view()->exists($this->_url) )
			{
				/* Rechercher si une vue existe */
				$view_params["view"] = $this->_url;
				
				/* Rechercher si l'url passée correspond à une méthode du controller de l'application */
				$method = $view_params["view"];
				if (method_exists($app, $method))
				{
					$view_params = $app->$method($view_params, $this->_func, $this->_params);
				}

				if(!isset($view_params['datas'])){$view_params['datas'] = [];}
				$view_params['datas'] = array_merge($this->_blog_datas, $view_params['datas']);
				
				if($view_params){ return $view_params; }
			}
			elseif (method_exists($app, $this->_url))
			{
				$method = $this->_url;

				$view_params = $app->$method($view_params, $this->_func, $this->_params);
				if(!isset($view_params['datas'])){$view_params['datas'] = [];}
				$view_params['datas'] = array_merge($this->_blog_datas, $view_params['datas']);
				if($view_params){return $view_params; }
			}



			/* 
				Methode 2
				L'url correspond-elle à un controleur existant et le premier paramètre à une methode de ce controleur ?
				methode 2 est placée avant methode 1, cela permet de réserver des urls dédiés sans risque de les faire écrasé par l'url d'un post

			*/

			if($this->_url != "")
			{
				$class_name = "\\App\\Controllers\\" . ucfirst($this->_url) . 'Controller';
			}
			
			if (class_exists($class_name))
			{
				/* Eventuellement détecter s'il y a conflit d'url avec un post dont l'url serait identique */


				/*
					Si un controleur existe pour cette vue on l'instancie dynamiquement en fonction de ce qu'on trouve dans l'url
					/{CONTROLEUR}/{METHODE}/{VARIABLE}
					METHODE ET VARIABLES ne sont pas obligatoires
					S'il n'y a pas de controleur, on vérifie s'il existe une vue, si la vue n'existe pas on déclenche une erreur 404
				*/
				
				
				if (class_exists($class_name))
				{


					$ctrlr = new $class_name($this->_conn);

					$datas = $ctrlr->{$this->_func['function']}($this->_func['var']);

					if($datas)
					{
						/* Variables de la vue */
						foreach ($datas['datas'] as $key => $val)
						{
							$bladeDatas[$key] = $val;
						}

						$view_params["datas"] = $bladeDatas;

						/* Surcharge vue en provenance du controleur */
						if(isset($datas['view']))
						{
							$view_params["view"]= $datas['view'];
						}
						
						/* message flash pour la vue courante */
						if(isset($datas['flash']))
						{
							$view_params["flash"]= $datas['flash'];
						}

						/* redirection de page provoqué par le controleur */
						if(isset($datas['redirect'])) 
						{
							$view_params["redirect"] = $datas['redirect'];	
						}
					}

					if(!isset($view_params['datas'])){$view_params['datas'] = [];}
					$view_params['datas'] = array_merge($this->_blog_datas, $view_params['datas']);
					if($view_params){  return $view_params; }
				}
				return false;
			}

			/* 
				Methode 3
				Rechercher une URL libre en base de données => cas du frontend  methode processUrl 
				todo: Déporter les url dans une table dédiée 
			*/
			$view_params = $app->processUrl($this->_url);
			if(!isset($view_params['datas'])){$view_params['datas'] = [];}
			$view_params['datas'] = array_merge($this->_blog_datas, $view_params['datas']);
			if($view_params){return $view_params; }


			

			return false;
		}

		public function setApp($val)
		{
			$this->_app = $val;
		}

		public function app()
		{
			return $this->_app;
		}

		public function setConn($val)
		{
			$this->_conn = $val;
		}

		public function conn()
		{
			return $this->_conn;
		}

		public function setMethod($val)
		{
			$this->_method = $val;
		}

		public function method()
		{
			return $this->_method;
		}

		private function setParams($val)
		{
			$this->_params = $val;
		}

		public function params()
		{
			return $this->_params;
		}

		public function url()
		{
			return $this->_url;
		}

		private function setUrl($val)
		{
			$this->_url = $val;
		}

		private function setFunc($val)
		{
			$val['function'] = !is_null($val['function']) ? $val['function'] : 'index';
			$this->_func = $val;
		}

		public function func()
		{
			return $this->_func;
		}

		
		public function setBlade($val)
		{
			$this->_blade = $val;
		}

		public function blade()
		{
			return $this->_blade;
		}
	}