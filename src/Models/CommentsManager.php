<?php

	/*
		class CommentsManager

	*/

	namespace App\Models;

	use \App\Comment;
	use \App\Models\PostsManager;
	use \App\Models\CommentManager;
	use \PDO;
	
	class CommentsManager extends Model
	{
		public $_commentsList = [];
		
		public function __construct($conn)
		{
			parent::__construct($conn);
		}


		public function selectAll($options)
		{
			$offset = ($this->currentPage() - 1) * $this->pagination();

			switch($options['filter'])
			{
				case "reported":
					$where = "where T03_report_i>0";
					break;

				case "inactive":
					$where = "where T03_state_va='inactive'";
					break;

				case "active":
					$where = "where T03_state_va='active'";
					break;
			}

			$query = "select T03_codeinterne_i, T02_codeinterne_i, T03_parent_i, T03_created_d, T03_ip_va, T03_title_va, T03_content_va, T03_name_va, T03_email_va, T03_state_va, T03_report_i FROM T03_comment $where order by T03_created_d desc ";
			echo $query;
			if(!is_null($this->pagination()))
			{
				$query .= " LIMIT " . $offset . ", " . $this->pagination();
			}
			
			$request = $this->_conn->prepare($query);
			$request->execute();
		    $comments = $request->fetchAll(PDO::FETCH_OBJ);
		    $request->closeCursor(); // Obligatoire car erreur SQLSTATE[HY000]: General error: 2014 Cannot execute queries while other unbuffered queries are active.

		    if($comments)
      		{
        		$_comments = [];
      			foreach($comments as $comment)
      			{
      				$c = new Comment($comment);
      				$this->setPost($c);
      				$_comments[] = $c;
      			}

        		return $_comments;
        	}

		    return false;
		}

		public function selectByPost($codeinterne, $options)
		{
			$state = isset($options['state']) ? $options['state'] : 'active';

			$query = $this->_conn->prepare("select T03_codeinterne_i, T02_codeinterne_i, T03_parent_i, T03_created_d, T03_ip_va, T03_title_va, T03_content_va, T03_name_va, T03_email_va, T03_state_va, T03_report_i FROM T03_comment where T02_codeinterne_i=:codeinterne and T03_state_va=:state");
			$query->bindValue(':codeinterne', $codeinterne);
			$query->bindValue(':state', $state);
			$query->execute();

		    $comments = $query->fetchAll(PDO::FETCH_ASSOC);
		    
		    if($comments)
      		{
        		$this->_commentsList = null;

				$ref = [];
		        foreach ($comments as $id => &$node) 
		        {
		            
		        	$c = new Comment($node);
      				//$node = $this->setPost($c);

		            // Use id as key to make a references to the tree and initialize it with node reference.
		            $ref[$node['T03_codeinterne_i']] = &$node;

		            // Add empty array to hold the children/subcategories
		            $node['children'] = [];

		            // Get your root node and add this directly to the tree
		            if ($node['T03_parent_i']==0) {
		                $this->_commentsList[$node['T03_codeinterne_i']] = &$node;
		            } else {
		                // Add the non-root node to its parent's references
		                $ref[$node['T03_parent_i']]['children'][$node['T03_codeinterne_i']] = &$node;
		            }
		        }
		        return $this->_commentsList;
        	}

		    return false;
		}


		public function get($codeinterne)
		{
			
			$codeinterne = (int) $codeinterne;

		    $request = $this->_conn->prepare('SELECT T02_codeinterne_i, T03_codeinterne_i, T03_parent_i, T03_created_d, T03_ip_va, T03_title_va, T03_content_va, T03_name_va, T03_email_va, T03_state_va, T03_report_i FROM T03_comment WHERE T03_codeinterne_i=:codeinterne');
		   
		    $request->bindValue(':codeinterne', $codeinterne);
			$request->execute();

		    $comment = $request->fetch(PDO::FETCH_ASSOC);

		    if($comment)
		    {
		    	$c = new Comment($comment);
      			$comment = $this->setPost($c);

		    	return $comment;
		    }
		    return false;
		    
		}

		

		public function update(Comment $comment)
		{
			$query = 'update T03_comment set T02_codeinterne_i=:T02_codeinterne_i, T03_parent_i=:T03_parent_i, T03_created_d=:T03_created_d, T03_ip_va=:T03_ip_va, T03_title_va=:T03_title_va, T03_content_va=:T03_content_va, T03_name_va=:T03_name_va, T03_email_va=:T03_email_va, T03_state_va=:T03_state_va, T03_report_i=:T03_report_i WHERE T03_codeinterne_i=:T03_codeinterne_i';
			
			$request = $this->_conn->prepare($query);
			$request->bindValue(':T03_codeinterne_i', $comment->T03_codeinterne_i);
			$request->bindValue(':T02_codeinterne_i', $comment->T02_codeinterne_i);
			$request->bindValue(':T03_parent_i', $comment->T03_parent_i);
			$request->bindValue(':T03_created_d', $comment->T03_created_d);
			$request->bindValue(':T03_ip_va', $comment->T03_ip_va);
			$request->bindValue(':T03_title_va', $comment->T03_title_va);
			$request->bindValue(':T03_content_va', $comment->T03_content_va);
			$request->bindValue(':T03_name_va', $comment->T03_name_va);
			$request->bindValue(':T03_email_va', $comment->T03_email_va);
			$request->bindValue(':T03_state_va', $comment->T03_state_va);
			$request->bindValue(':T03_report_i', $comment->T03_report_i);
			$request->execute();

				
			return $comment;
		}

		public function create(array $fields, $type)
		{
			$sql = 'INSERT INTO T03_comment (T02_codeinterne_i, T03_parent_i, T03_created_d, T03_ip_va, T03_title_va, T03_content_va, T03_name_va, T03_email_va, T03_state_va) VALUES(:T02_codeinterne_i, :T03_parent_i, :T03_created_d, :T03_ip_va, :T03_title_va, :T03_content_va, :T03_name_va, :T03_email_va, :T03_state_va)';

			$query = $this->_conn->prepare($sql);

			$query->bindValue(':T02_codeinterne_i' , $fields['T02_codeinterne_i']);
			$query->bindValue(':T03_parent_i' , $fields['T03_parent_i']);
			$query->bindValue(':T03_created_d' , $fields['T03_created_d']);
			$query->bindValue(':T03_ip_va' , $fields['T03_ip_va']);
			$query->bindValue(':T03_title_va' , $fields['T03_title_va']);
			$query->bindValue(':T03_content_va' , $fields['T03_content_va']);
			$query->bindValue(':T03_name_va' , $fields['T03_name_va']);
			$query->bindValue(':T03_email_va' , $fields['T03_email_va']);
			$query->bindValue(':T03_state_va' , "active");
			
			$query->bindValue($params);	


			$result = $query->execute();

			$comment = new Comment($fields);

			$comment->hydrate([
			  'T03_codeinterne_i' => $this->_conn->lastInsertId()
			]);

			return $this->_conn->lastInsertId();
		}

		public function delete($codeinterne)
		{
			$codeinterne = (int) $codeinterne;
			$comments = $this->selectByPost($codeinterne);
			$query = $this->_conn->prepare('DELETE FROM T03_comment WHERE T03_codeinterne_i=:codeinterne');
		    $query->bindValue(':codeinterne', $codeinterne);
			$query->execute();
		}


		public function deleteFromPost($codeinterne)
		{
			$codeinterne = (int) $codeinterne;
			$comments = $this->selectByPost($codeinterne);
			$query = $this->_conn->prepare('DELETE FROM T03_comment WHERE T02_codeinterne_i=:codeinterne');
		    $query->bindValue(':codeinterne', $codeinterne);
			$query->execute();
		}



		public function setPost(Comment $comment)
		{
			$postManager = new PostsManager($this->_conn);
			$post = $postManager->get($comment->T02_codeinterne_i);

			if($post)
			{
				$comment->setPost($post);
				return $comment;
			}
			return false;

		}


		public function count($status = null )
		{
			if($status == "reported")
			{
				$query = "select count(*)nb FROM T03_comment where T03_report_i>0";
			}
			else
			{
				$query = "select count(*)nb FROM T03_comment";

				if(!is_null($status))
				{
					$query .= " WHERE T03_state_va=:status ";
				}
			}

			$request = $this->_conn->prepare($query);

		    if(!is_null($status)) {$request->bindValue(':status', $status);}

		    $request->execute();
		    $comment = $request->fetch(PDO::FETCH_OBJ);
		    $request->closeCursor();

		    if($comment)
      		{
        		return $comment->nb;
        	}

		    return false;
		}
	}