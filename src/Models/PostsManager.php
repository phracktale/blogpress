<?php
	/*
			class PostsManager

	*/

	namespace App\Models;

	use \App\Post;
	use \App\Models\UserManager;
	use \PDO;

	class PostsManager extends Model
	{
		public $_postsList;
		
		public function __construct($conn)
		{
			parent::__construct($conn);
		}

		
		public function selectAll($type = "post", $status = null )
		{
			$offset = ($this->_currentPage - 1) * $this->_pagination;

			$query = "select T02.T02_codeinterne_i, T01.T01_email_va, T01.T01_firstname_va, T01.T01_lastname_va, T01.T01_codeinterne_i, T02.T02_logo_va, T02.T02_url_va, T02.T02_title_va, T02.T02_subtitle_va, T02.T02_abstract_va, T02.T02_content_va, T02.T02_type_va, T02.T02_status_va, T02.T02_creation_d, T02.T02_update_d, T02.T02_publication_d FROM T02_post T02 left join T01_user T01 on T01.T01_codeinterne_i=T02.T01_codeinterne_i WHERE T02_type_va=:type ";


			if(!is_null($status))
			{
				$query .= " and T02_status_va=:status ";
			}
			
			$query .= " order by T02_creation_d DESC ";

			if(!is_null($this->_pagination))
			{
				$query .= " LIMIT " . $offset . ", " . $this->_pagination;
			}
			
			$request = $this->_conn->prepare($query);

		    if(!is_null($status)) {$request->bindValue(':status', $status);}

		    $request->bindValue(':type', $type);
		    $request->execute();
		    $posts = $request->fetchAll(PDO::FETCH_OBJ);
		    $request->closeCursor(); // Pas besoin ça fonctionne sans, dans commentsManager, il est obligatoire par contre sinon plante

		    if($posts)
      		{
      			$_posts = [];
      			foreach($posts as $post)
      			{
      				$p = new Post($post);
      				$post = $this->setUser($p);
      				$_posts[] = $post;
      			}
        		return $_posts;
        	}

		    return false;
		}

		public function get($codeinterne)
		{
			
			$codeinterne = (int) $codeinterne;

		    $q = $this->_conn->query('SELECT T02.T02_codeinterne_i, T01.T01_email_va, T01.T01_firstname_va, T01.T01_lastname_va, T01.T01_codeinterne_i, T02.T02_logo_va, T02.T02_url_va, T02.T02_title_va, T02.T02_subtitle_va, T02.T02_abstract_va, T02.T02_content_va, T02.T02_type_va, T02.T02_status_va, T02.T02_creation_d, T02.T02_update_d, T02.T02_publication_d FROM T02_post T02 left join T01_user T01 on T01.T01_codeinterne_i=T02.T01_codeinterne_i WHERE T02_codeinterne_i = '.$codeinterne);

		    $post = $q->fetch(PDO::FETCH_ASSOC);
		    if($post)
		    {
		    	$p = new Post($post);
      			$post = $this->setUser($p);
		    	return $post;
		    }

		    return false;
		    
		}

		public function create(array $fields,$type)
		{
			$sql = 'INSERT INTO T02_post (T01_codeinterne_i, T02_parent_i, T02_title_va, T02_subtitle_va, T02_abstract_va, T02_content_va, T02_logo_va, T02_url_va, T02_status_va, T02_type_va, T02_creation_d, T02_update_d, T02_publication_d) VALUES(:T01_codeinterne_i, :T02_parent_i, :T02_title_va, :T02_subtitle_va, :T02_abstract_va, :T02_content_va, :T02_logo_va, :T02_url_va, :T02_status_va, :T02_type_va, :T02_creation_d, :T02_update_d, :T02_publication_d)';

			$query = $this->_conn->prepare($sql);

			$status = $fields['T02_status_va'] == "on" ? "published": "draft";

			$query->bindValue(':T01_codeinterne_i' ,  $_SESSION['user']->T01_codeinterne_i());
			$query->bindValue(':T02_parent_i' , $fields['T02_parent_i']);
			$query->bindValue(':T02_title_va' , $fields['T02_title_va']);
			$query->bindValue(':T02_subtitle_va' , $fields['T02_subtitle_va']);
			$query->bindValue(':T02_abstract_va' , $fields['T02_abstract_va']);
			$query->bindValue(':T02_content_va' , $fields['T02_content_va']);
			$query->bindValue(':T02_logo_va' , $fields['T02_logo_va']);
			$query->bindValue(':T02_url_va' , $fields['T02_url_va']);
			$query->bindValue(':T02_status_va' , $status);
			$query->bindValue(':T02_type_va' , $fields['T02_type_va']);
			$query->bindValue(':T02_creation_d' , $fields['T02_creation_d']);
			$query->bindValue(':T02_update_d' , $fields['T02_update_d']);
			$query->bindValue(':T02_publication_d' , $fields['T02_publication_d']);
			
			$query->bindValue($params);	


			$result = $query->execute();

			$post = new Post($fields);
			$post->hydrate([
			  'T02_codeinterne_i' => $this->_conn->lastInsertId()
			]);

			return $post;
		}

		public function update(Post $post)
		{
			$query = 'update T02_post set T02_title_va=:T02_title_va, T02_subtitle_va=:T02_subtitle_va, T02_abstract_va=:T02_abstract_va, T02_content_va=:T02_content_va, T02_logo_va=:T02_logo_va, T02_url_va=:T02_url_va, T02_status_va=:T02_status_va, T02_update_d=:T02_update_d, T02_publication_d=:T02_publication_d WHERE T02_codeinterne_i=:T02_codeinterne_i';
			
			
			$query = $this->_conn->prepare($query);
			$query->bindValue(':T02_title_va', $post->T02_title_va);
			$query->bindValue(':T02_subtitle_va', $post->T02_subtitle_va);
			$query->bindValue(':T02_abstract_va', $post->T02_abstract_va);
			$query->bindValue(':T02_content_va', $post->T02_content_va);
			$query->bindValue(':T02_logo_va', $post->T02_logo_va);
			$query->bindValue(':T02_url_va', $post->T02_url_va);
			$query->bindValue(':T02_status_va', $post->T02_status_va);
			$query->bindValue(':T02_update_d', $post->T02_update_d);
			$query->bindValue(':T02_publication_d', $post->T02_publication_d);
			$query->bindValue(':T02_codeinterne_i', $post->T02_codeinterne_i);
			$query->execute();
			
			return $post;
		}

		

		public function delete($codeinterne)
		{
			$query = $this->_conn->prepare("delete from T02_post where T02_codeinterne_i=:T02_codeinterne_i");
			$query->bindValue(':T02_codeinterne_i' , $codeinterne);
			$query->bindValue($params);	
			$result = $query->execute();
			return $result;
		}

		public function setUser(Post $post)
		{
			$userManager = new UserManager($this->_conn);
			$user = $userManager->get($post->T01_codeinterne_i);
			$post->setUser($user);
			return $post;
		}


		public function getByUrl($url)
		{
			$query = 'SELECT T02.T02_codeinterne_i, T01.T01_email_va, T01.T01_firstname_va, T01.T01_lastname_va, T01.T01_codeinterne_i, T02.T02_logo_va, T02.T02_url_va, T02.T02_title_va, T02.T02_subtitle_va, T02.T02_abstract_va, T02.T02_content_va, T02.T02_type_va, T02.T02_status_va, T02.T02_creation_d, T02.T02_update_d, T02.T02_publication_d FROM T02_post T02 left join T01_user T01 on T01.T01_codeinterne_i=T02.T01_codeinterne_i WHERE T02_url_va=:url';

		    $sth = $this->_conn->prepare($query);
		    $sth->bindValue(':url', $url);
		    $sth->execute();
		    $post = $sth->fetch(PDO::FETCH_ASSOC);

		    if($post)
		    {
		    	$p = new Post($post);
      			$post = $this->setUser($p);
		    	
		    	return $post;
		    }
		    return false;
		    
		}

		
		public function count($type = "post", $status = null )
		{
			$query = "select count(*)nb FROM T02_post T02 WHERE T02_type_va=:type ";

			if(!is_null($status))
			{
				$query .= " and T02_status_va=:status ";
			}
			
			$request = $this->_conn->prepare($query);

		    if(!is_null($status)) {$request->bindValue(':status', $status);}

		    $request->bindValue(':type', $type);
		    $request->execute();
		    $post = $request->fetch(PDO::FETCH_OBJ);
		    $request->closeCursor(); // Pas besoin ça fonctionne sans, dans commentsManager, il est obligatoire par contre sinon plante

		    if($post)
      		{
        		return $post->nb;
        	}

		    return false;
		}
	}