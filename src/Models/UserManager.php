<?php

	/*
		class User

	*/

	namespace App\Models;

	use \App\User;
	use \PDO;
	
	class UserManager extends Model
	{
		public function __construct($conn)
		{
			parent::__construct($conn);
		}

		public function login($login, $password)
		{
			$query = "select * from T01_user where T01_login_va=:T01_login_va ;";
			$sth = $this->_conn->prepare($query);
		    $sth->bindValue(':T01_login_va', $login);
		    $sth->execute();
		    $result = $sth->fetch(PDO::FETCH_OBJ);

		    if($result)
      		{
        		if(md5($password) == $result->T01_password_va)
        		{
        			$fields = [
        				'T01_codeinterne_i'	=> $result->T01_codeinterne_i,
        				'T01_login_va'		=> $result->T01_login_va,
        				'T01_email_va'		=> $result->T01_email_va,
        				'T01_password_va'	=> $result->T01_password_va,
        				'T01_firstname_va'	=> $result->T01_firstname_va,
        				'T01_lastname_va'	=> $result->T01_lastname_va,
        				'T01_created_d'		=> $result->T01_created_d,
        				'T01_lastlog_d'		=> $result->T01_lastlog_d,
        			];

        			return new User($fields);
        		}
        	}
		    return false;
		}

		public function create()
		{
			
		}

		public function selectAll()
		{

		}

		public function get($codeinterne)
		{
			$request = $this->_conn->prepare("select T01_codeinterne_i, T01_login_va, T01_email_va, T01_password_va, T01_firstname_va, T01_lastname_va, T01_created_d, T01_lastlog_d from T01_user where T01_codeinterne_i=:codeinterne ;");
		    $request->bindValue(':codeinterne', $codeinterne);
		    $request->execute();
		    $user = $request->fetch(PDO::FETCH_OBJ);

		    if($user)
      		{
      			return new User($user);
      		}
      		return false;
		}

		public function update(User $user)
		{
			$query = 'update T01_user set T01_login_va=:T01_login_va, T01_email_va=:T01_email_va, T01_password_va=:T01_password_va, T01_firstname_va=:T01_firstname_va, T01_lastname_va=:T01_lastname_va WHERE T01_codeinterne_i=:T01_codeinterne_i';

			$query = $this->_conn->prepare($query);
			$query->bindValue(':T01_codeinterne_i', $user->T01_codeinterne_i);
			$query->bindValue(':T01_login_va', $user->T01_login_va);
			$query->bindValue(':T01_email_va', $user->T01_email_va);
			$query->bindValue(':T01_password_va', $user->T01_password_va);
			$query->bindValue(':T01_firstname_va', $user->T01_firstname_va);
			$query->bindValue(':T01_lastname_va', $user->T01_lastname_va);
			$query->execute();
			
			return $user;
		}

		public function delete($codeinterne)
		{

		}

	}