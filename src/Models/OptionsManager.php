<?php

	/*
		class OptionsManager

	*/

	namespace App\Models;

	use \App\Comment;
	use \App\Models\PostsManager;
	use \App\Models\CommentManager;
	use \PDO;
	
	class OptionsManager extends Model
	{
		
		public function __construct($conn)
		{
			parent::__construct($conn);
		}

		public function getOption($key)
		{
			$query = $this->_conn->prepare("select T04_codeinterne_i, T04_key_va, T04_value_va FROM T04_options where T04_key_va =:T04_key_va");
			$query->bindValue(':T04_key_va', $key);
			$query->execute();

		    $option = $query->fetch(PDO::FETCH_OBJ);
		   
		    if($option)
      		{
      			return $option;
        	}

		    return false;
		}

		public function get($codeinterne)
		{
			
			$codeinterne = (int) $codeinterne;

		    $query = $this->_conn->prepare("select T04_codeinterne_i, T04_key_va, T04_value_va FROM T04_options where T04_codeinterne_i =:T04_codeinterne_i");
			$query->bindValue(':T04_codeinterne_i', $T04_codeinterne_i);
			$query->execute();

		    $option = $query->fetch(PDO::FETCH_ASSOC);
		    
		    if($option)
      		{
      			return $option;
        	}

		    return false;
		    
		}

		public function updateKey($key, $value)
		{
			$query = $this->_conn->prepare("update T04_options set T04_value_va=:T04_value_va where T04_key_va=:T04_key_va;");
			$query->bindValue(':T04_key_va', $key);
			$query->bindValue(':T04_value_va', $value);
			$result = $query->execute();
			return $result;
		}

		public function update(Option $option)
		{
			
		}

		public function create(array $fields, $type)
		{
			
		}

		public function delete($codeinterne)
		{
			
		}

	}