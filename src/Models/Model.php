<?php

	/*
		class Model

	*/

	namespace App\Models;

	class Model  
	{
		public $_conn;
		public $_postsList = null;
		public $_pagination = 5;
		public $_currentPage = 1;
		
		public function __construct($conn)
		{
			$this->setDb($conn);
		}
		
		private function setDb($conn)
		{
			$this->_conn = $conn;
		}

		public function pagination()
		{
			return $this->_pagination;
		}

		public function setPagination($val)
		{
			$this->_pagination = $val;
		}

		public function currentPage()
		{
			return $this->_currentPage;
		}

		public function setCurrentPage($val)
		{
			$this->_currentPage = $val;
		}

	}