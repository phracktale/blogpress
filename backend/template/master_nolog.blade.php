<!DOCTYPE html>
<html>
  <head>
    <title>Blog press - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="UTF-8" />

    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ $assets_url }}css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ $assets_url }}css/animate.min.css" />
    <link rel="stylesheet" href="{{ $assets_url }}css/bootstrap-checkbox.css" />
    <link rel="stylesheet" href="{{ $assets_url }}css/chosen.min.css" />
    <link rel="stylesheet" href="{{ $assets_url }}css/chosen-bootstrap.css" />
   <link rel="stylesheet" href="{{ $assets_url }}css/minoral.css" />
    <link rel="stylesheet" href="{{ $assets_url }}css/perso.css" />
    <link rel="stylesheet" href="{{ $assets_url }}js/plugins/tabdrop/css/tabdrop.css" />
    <link rel="stylesheet" href="{{ $base_url }}vendor/enyo/dropzone/dist/min/dropzone.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="brownish-scheme">

    <!-- Preloader -->
    <div class="mask"><div id="loader"></div></div>
    <!--/Preloader -->

    <!-- Wrap all page content here -->
    <div id="wrap">
      <!-- Make page fluid -->
      <div class="row">
        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
          


          <!-- Branding -->
          <div class="navbar-header col-md-2">
            <a class="navbar-brand" href="index.html">
              <strong>BLOG</strong> <span class="divider vertical"></span> PRESS
            </a>
            <div class="sidebar-collapse">
              <a href="#">
                <i class="fa fa-bars"></i>
              </a>
            </div>
          </div>
          <!-- Branding end -->


          <!-- .nav-collapse -->
          <div class="navbar-collapse">

            <!-- Content collapsing at 768px to sidebar -->
            <div class="collapsing-content">
            </div>
            <!-- /Content collapsing at 768px to sidebar -->



            <!-- Sidebar -->
            <ul class="nav navbar-nav side-nav" id="navigation">
              <li class="collapsed-content">
                <!-- Collapsed content pasting here at 768px -->
              </li>
             
            </ul>
            <!-- Sidebar end -->
          </div>
          <!--/.nav-collapse -->
        </div>
        <!-- Fixed navbar end -->
        <!-- Page content -->
        <div id="content" class="col-md-12">
          
          <!-- submenu -->
          
            <h2>@yield('title')</h2>
            
          <!-- /submenu -->
          <!-- content main container -->
          <div class="main">

             {!! $flash !!}

             @yield('content')

          </div>
          <!-- /content container -->
        </div>
        <!-- Page content end -->
      </div>
      <!-- Make page fluid-->
    </div>
    <!-- Wrap all page content end -->

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ $base_url }}vendor/components/jquery/jquery.js"></script>
    <script src="{{ $base_url }}vendor/components/jqueryui/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ $assets_url }}js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=sons-of-obsidian"></script>
    <script src="{{ $assets_url }}js/plugins/jquery.nicescroll.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/blockui/jquery.blockUI.js"></script>
    <script src="{{ $assets_url }}js/plugins/rickshaw/raphael-min.js"></script> 
    <script src="{{ $assets_url }}js/plugins/rickshaw/d3.v2.js"></script>
    <script src="{{ $assets_url }}js/plugins/rickshaw/rickshaw.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/skycons/skycons.js"></script>
    <script src="{{ $assets_url }}js/plugins/jquery.sparkline.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/tabdrop/bootstrap-tabdrop.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/morris/morris.min.js"></script>
    <script src="{{ $assets_url }}js/minoral.js"></script>
    <script src="{{ $base_url }}vendor/tinymce/tinymce/tinymce.min.js"></script>
    <script src="{{ $base_url }}vendor/enyo/dropzone/dist/min/dropzone.min.js"></script>

  </body>
</html>