@extends('master')
@section('title')
Edition de l'article
@endsection
@section('content')
<!-- CONTENT ARTICLE -->
<form class="form-horizontal" role="form" method="POST" action="{{ $base_url_backend }}articles/update/{{ $T02_codeinterne_i }}">

  <input type="hidden" id="hash" name="hash" value="{{ $hash }}" />
  <input type="hidden" id="T02_logo_va" name="T02_logo_va" value="{{ $T02_logo_va }}" />
  @include('parts.postForm')
  
</form>
<!-- CONTENT ARTICLE -->
@endsection

@section('scripts')

  tinymce.init({
    selector: '#T02_abstract_va'
  });

  tinymce.init({
    selector: '#T02_content_va'
  });

  var myLogo = new Dropzone('#logo', {
        url: "{{ $base_url_backend }}upload_logo_article",
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 20,
         autoQueue: true,
        clickable: "#add_logo"
      });

   myLogo.on("success", function(file, response ){
        alert('ok');
        obj = JSON.parse(response);
        var d = new Date();
        var n = d.getTime();
        $('#logo').find('img').attr('src', '{{ $base_url }}uploads/' + obj.targetFile + '?' + n);
        $('#T02_logo_va').val('{{ $base_url }}uploads/' + obj.targetFile);
      });
@endsection