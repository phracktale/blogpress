@extends('master')

@section('title')
  <h2>Dashboard blogpress</h2>
  <p></p>
@endsection

@section('content')
    <section class="tile cornered">
    	<div class="tile-header">
	        <h3><strong>Statistiques</strong> Contenus</h3>
	        <div class="controls">
	          <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
	          <a href="#" class="remove"><i class="fa fa-times"></i></a>
	        </div>
	      </div>
	    <div class="tile-body">
	    	<div class="row">
				<div class="col-lg-3 col-md-6">

				    <div class="panel panel-primary">
				      <div class="panel-heading">
				        <h4 class="panel-title">Articles</h4>
				        	
				      </div>
				      <div class="panel-body">
				       	{!! $articles_stats !!}
				      </div>
				    </div>
			  	</div>

			  	<div class="col-lg-3 col-md-6">

				    <div class="panel panel-primary">
				      <div class="panel-heading">
				        <h4 class="panel-title">Commentaires</h4>
				        
				      </div>
				      <div class="panel-body">
				       	{!! $commentaires_stats !!}
				      </div>
				    </div>
			  	</div>
			</div>
			
		</div>

   </section>

    <!-- Commentaires signalés -->
    @if($comments)

   	<div class="row">
	  <div class="col-md-12">

		<section class="tile cornered color reported_comments">

		  <div class="tile-header">
		    <h1><strong>Commentaires</strong> signalés</h1>
		    <div class="controls">
		      <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
		      <a href="#" class="remove"><i class="fa fa-times"></i></a>
		    </div>
		  </div>
		  <div class="tile-body nopadding">
		   @include('parts/comments', array('force_valider' => true, 'comments' => $comments))
		  </div>

		</section>
		  
	  </div>
  	</div>
  	@endif

@endsection

@section('script')
 
@endsection