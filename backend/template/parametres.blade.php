@extends('master')

@section('title')
  <h2>Paramètres blogpress</h2>
  <p></p>
@endsection

@section('content')
    <section class="tile cornered">
    	<div class="tile-header">
	        <div class="controls">
	          <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
	          <a href="#" class="remove"><i class="fa fa-times"></i></a>
	        </div>
	      </div>
	    <div class="tile-body">
	    	<div class="row">
				<div class="col-lg-9">

				    <div class="panel panel-primary">
				      <div class="panel-heading">
				        <h3 class="panel-title">Général</h3>
				        
				        </form>
				        
				      </div>
				      <div class="panel-body">
				       	<form method="POST" class="form-horizontal">
				       		<input type="hidden" name="hash" value="{{ $hash }}" />
					        <div class="form-group">
						        <label for="blog_name" class="col-sm-4 control-label">Titre du blog</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="blog_name" name="blog_name" value="{{ $blog_name }}" />
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="blog_name" class="col-sm-4 control-label">Sous-Titre du blog</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="blog_slogan" name="blog_slogan" value="{{ $blog_slogan }}" />
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="blog_email" class="col-sm-4 control-label">Email de contact</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="blog_email" name="blog_email" value="{{ $blog_email }}" />
						       	</div>
					        </div>

				        	<p class="text-right"><input type="submit" value="sauvegarder" class="btn btn-primary" /></p>
				      </div>
				    </div>
			  	</div>

	
			</div>
			
		</div>

   </section>

  </div>

@endsection

@section('script')
 
@endsection