@extends('master')

@section('title')
  {{ $title }}
@endsection

@section('content')
  <!-- CONTENT ARTICLE -->
  <div class="col-md-9">
    <p>
    <a href="{{ $base_url_backend }}liste_articles/filtre/tous" class="btn btn-xs btn-default">Tous les articles ({{ $nb_posts }})</a>
    <a href="{{ $base_url_backend }}liste_articles/filtre/publies" class="btn btn-xs btn-default">Publiés ({{ $nb_published }})</a>
    <a href="{{ $base_url_backend }}liste_articles/filtre/brouillons" class="btn btn-xs btn-default">Brouillons ({{ $nb_draft }})</a>
    </p>
    <table class="table  table-bordered table-sortable">
      <thead>
        <tr>
          <td>ID</td>
          <th>Logo</th>
          <th>Titre</th>
          <th>Auteur</th>
          <th>Publication</th>
          <td></td>
        </tr>
      </thead>
      <tbody>
    @foreach ($posts as $post)
      @php
        $date_creation = date('d/m/Y h:i:s', strtotime($post->T02_creation_d));
        $status = $post->T02_status_va == "published" ? "Publié" : "Brouillon";
        if($status == "Brouillon"){$row_class = "red";}
        if($status == "Publié"){$row_class = "active";}
      @endphp
        <tr class="{{ $row_class }}">
          <td>
           {{ $post->T02_codeinterne_i }}
           </td>
           <td>
           <div class="thumbnail thumbnail_100">
           @if($post->T02_logo_va != "")
            
              <img src="{{ $post->T02_logo_va }}" alt="logo de l'article" />
            
           @else
            <img src="http://fakeimg.pl/100x100/?text=Logo%20article" alt="logo de l'article" />
            @endif
            </div>
          </td>
          <td>
            {{ $post->T02_title_va }}
          </td>
          <td>
            {{ $post->User->T01_firstname_va }} {{ $post->User->T01_lastname_va }} 
          </td>
          <td>
            {{ $status }}
            <br/>Le {{ $date_creation }}
          </td>
          <td class="text-right">
            <p><a href="{{ $base_url_backend }}articles/get/{{ $post->T02_codeinterne_i }}" class="btn btn-xs btn-info">Editer l'article </a></p>
            <p><a href="{{ $base_url_backend }}articles/delete/{{ $post->T02_codeinterne_i }}" class="btn btn-xs btn-warning">Supprimer l'article </a></p>
          </td>
        </tr>
    @endforeach
    </tbody>
    </table>

  </div>
  <div class="col-md-3">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">

      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">
        
          <a href="{{ $base_url_backend }}articles/create" class="btn btn-primary btn-lg">Nouvel article</a>
          <div class="clear"></div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection