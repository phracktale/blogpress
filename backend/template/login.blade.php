@extends('master_nolog')

@section('title')
  <h2>Accés à l'espace privé</h2>
@endsection

@section('content')
<div class="col-lg-4 welcome">
	<form id="form-signin" class="form-signin" method="post" action="{{ $base_url_backend }}identification">
		<input type="hidden" name="hash" value="{{ $hash }}">
	  <section>
	    <div class="form-group">
	    	<label for="login">Identifiant</label>
	    	<div class="input-group">
	      		<input type="text" class="form-control" name="login" placeholder="Identifiant">
	      		<div class="input-group-addon"><i class="fa fa-user"></i></div>
	    	</div>
	    </div>
	    
	    <div class="form-group">
	    	<label for="password">Mot de passe</label>
	    	<div class="input-group">
	      		<input type="password" class="form-control" name="password" placeholder="mot de passe">
	      		<div class="input-group-addon"><i class="fa fa-key"></i></div>
	    	</div>
	    </div>
	  </section>
	  
	  <p class="text-right"><button type="submit" class="log-in"><i class="fa fa-arrow-right fa-5x"></i> S'identifier</button></p>
	</form>
</div>
@endsection


