@extends('master')

@section('title')
  Liste des commentaires
@endsection

@section('content')
 <p>
    <a href="{{ $base_url_backend }}liste_commentaires/filtre/tous" class="btn btn-xs btn-default">Tous les commentaires ({{ $nb_comments }})</a>
    <a href="{{ $base_url_backend }}liste_commentaires/filtre/publies" class="btn btn-xs btn-default">Validés ({{ $nb_active }})</a>
    <a href="{{ $base_url_backend }}liste_commentaires/filtre/rejetes" class="btn btn-xs btn-default">Rejetés ({{ $nb_inactive }})</a>
    <a href="{{ $base_url_backend }}liste_commentaires/filtre/signales" class="btn btn-xs btn-default">Signalés ({{ $nb_reported }})</a>
    </p>
 <!-- Commentaires signalés -->
  <div class="col-md-9">
  <h3>Tous les commentaires</h3>
  @include('parts/comments', array('comments' => $comments))
  </div>
  <div class="col-md-3">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">

      </div>
      <!-- /tile header -->

      <!-- tile body -->
      <div class="tile-body">

      </div>
      <!-- /tile body -->
    </section>
  </div>
  <!-- CONTENT ARTICLE -->
@endsection