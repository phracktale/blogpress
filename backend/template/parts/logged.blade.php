<div class="navbar-form navbar-right">
	<span class="label label-default">{{ $email }}</span> <a href="{{ $base_url_backend }}/logout" class="btn btn-xs btn-warning">Se déconnecter</a>
</div>