
<table class="table table-bordered table-sortable">
      <thead>
        <tr>
          <td class="sortable sort-alpha">ID</td>
          <th class="sortable sort-alpha">Article</th>
          <th class="sortable sort-alpha">Commentaire</th>
          <th class="sortable sort-alpha">Auteur</th>
          <th class="sortable sort-alpha sort-desc">Date</th>
          <th class="sortable sort-alpha sort-desc">Nb de signalements</th>
          <td></td>
        </tr>
      </thead>
      <tbody>

    @foreach ($comments as $comment)
      @php
        $date_creation = date('d/m/Y h:i:s', strtotime($comment->T03_created_d));
     
          switch($comment->T03_state_va)
          {
            default:
            case "":
              $class = "warning";
              break;

            case "inactive":
              $class = "danger";
              break;

            case "active":
              $class = "info";
              break;
          }
      @endphp
       <tr class="{{ $class }}">
          <td>
          {{ $comment->T03_codeinterne_i }}
          </td>
          <td>
            <a href="{{ $base_url }}{{ $comment->Post->T02_url_va }}">{{ $comment->Post->T02_title_va }}</a>
            
          </td>
          <td>
            <h4>{{ $comment->T03_title_va }}</h4>
            <p>{{ $comment->T03_content_va }}</p>
          </td>
          <td>
            {{ $comment->T03_name_va }}
            <br/>{{ $comment->T03_email_va }}
            <br/>{{ $comment->T03_ip_va }}
          </td>
          <td>
            {{ $date_creation }} 
          </td>
          <td>
            {{ $comment->T03_report_i }} 
          </td>
          <td>
            @if (!$comment->T03_state_va || $comment->T03_state_va == "inactive" || $force_valider)
            <a href="{{ $base_url_backend }}{{ $controller }}comments/valider/{{ $comment->T03_codeinterne_i }}" class="btn btn-info btn-sm">Valider</a> 
            @endif
            @if (!$comment->T03_state_va || $comment->T03_state_va == "active")
            <a href="{{ $base_url_backend }}{{ $controller }}comments/rejeter/{{ $comment->T03_codeinterne_i }}" class="btn btn-warning btn-sm">Rejeter</a>
            @endif

            <!--
             @if ($comment->T03_state_va == "inactive")
            <a href="{{ $base_url_backend }}{{ $controller }}comments/supprimer/{{ $comment->T03_codeinterne_i }}" class="btn btn-danger btn-sm">Supprimer définitivement</a> 
            @endif
            -->
          </td>
        </tr>
    @endforeach
    </tbody>
    </table>