
  <div class="col-md-9">
    <!-- tile -->
    <section class="tile">
      <!-- tile widget -->
      <!-- tile body -->
      <div class="tile-body nopadding">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Article</h3>
          </div>
          <div class="panel-body">
            
            <div class="form-group">
              <label for="T02_title_va" class="col-sm-2 control-label">Titre</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="T02_title_va" id="T02_title_va" value="{{ $T02_title_va }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="T02_subtitle_va" class="col-sm-2 control-label">Sous-titre</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="T02_subtitle_va" id="T02_subtitle_va" value="{{ $T02_subtitle_va }}"  />
              </div>
            </div>
            <div class="form-group no-bottom-margin">
              <label for="T02_url_va" class="col-sm-2 control-label">URL</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="T02_url_va" id="T02_url_va" value="{{ $T02_url_va }}" />
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <!-- /tile body -->
      
      <!-- tile body -->
      <div class="tile-body nopadding">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Introduction</h3>
          </div>
          <div class="panel-body">
          <textarea name="T02_abstract_va" id="T02_abstract_va">{{ $T02_abstract_va }}</textarea></div>
        </div>
      </div>
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title">Texte</h3>
        </div>
        <div class="panel-body">
          <textarea name="T02_content_va" id="T02_content_va">{{ $T02_content_va }}</textarea>
        </div>
      </div>
    <!-- /tile body -->
    </section>
    <!-- /tile -->

  </div>
  <div class="col-md-3">
    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong>Publication</strong> </h1>
        
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <div class="tile-body">
        <div class="row">
          <div class="col-md-4">
              <div class="publiedraft onoffswitch labeled cyan">
                <input type="checkbox" name="T02_status_va" class="onoffswitch-checkbox" id="T02_status_va" {{ $statusChecked }}>
                <label class="onoffswitch-label" for="T02_status_va">
                  <div class="onoffswitch-inner"></div>
                  <div class="onoffswitch-switch"></div>
                </label>
              </div>
          </div>
          <div class="col-md-8 text-center"> 
            <button type="submit" class="btn btn-lg btn-info align-right">Sauver</button>
          </div>
        </div>
      </div>
      <!-- /tile body -->
    </section>


    <section class="tile cornered">
      <!-- tile header -->
      <div class="tile-header">
        <h1><strong>Logo</strong> </h1>
        
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <div class="tile-body">
        <div class="row">
          <div class="col-md-12">
            <div id="logo">
            @if($T02_logo_va != "")
              <img src="{{ $T02_logo_va }}" alt="Logo de l'article">
            @else
              <img src="http://fakeimg.pl/247x250/?text=Logo%20article" alt="logo de l'article" />
             @endif
            
            </div>
            <div class="text-center"><br>
  
          <input type="button" id="add_logo" class="btn btn-lg btn-info dz-clickable" value="Changer le logo d'article"></div>
          </div>
        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>