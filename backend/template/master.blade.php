<!DOCTYPE html>
<html>
  <head>
    <title>Blog|press</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />

    <link rel="icon" type="image/ico" href="images/favicon.ico" />
    <!-- Bootstrap -->
    <link href="{{ $assets_url }}css/bootstrap.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ $assets_url }}css/animate.min.css">
    <link rel="stylesheet" href="{{ $assets_url }}/css/rickshaw.min.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/bootstrap-checkbox.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/chosen.min.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/chosen-bootstrap.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/morris.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/minoral.css">
    <link rel="stylesheet" href="{{ $assets_url }}css/perso.css">
    <link rel="stylesheet" href="{{ $assets_url }}js/plugins/tabdrop/css/tabdrop.css">
    <link rel="stylesheet" href="{{ $base_url }}vendor/enyo/dropzone/dist/min/dropzone.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="brownish-scheme">

    <!-- Preloader -->
    <div class="mask"><div id="loader"></div></div>
    <!--/Preloader -->

    <!-- Wrap all page content here -->
    <div id="wrap">
      <!-- Make page fluid -->
      <div class="row">
        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
          


          <!-- Branding -->
          <div class="navbar-header col-md-2">
            <a class="navbar-brand" href="index.html">
              <strong>BLOG</strong> <span class="divider vertical"></span> PRESS
            </a>
            <div class="sidebar-collapse">
              <a href="#">
                <i class="fa fa-bars"></i>
              </a>
            </div>
          </div>
          <!-- Branding end -->


          <!-- .nav-collapse -->
          <div class="navbar-collapse">

            <!-- Content collapsing at 768px to sidebar -->
            <div class="collapsing-content">
              <ul class="nav navbar-nav">
               <li class="quick-action ">
                  <a class="button" href="{{ $base_url }}">
                    Voir le site public
                    <span class="overlay-label green">go!</span>
                  </a>
                  
                </li>

              </ul>
              <!-- User Controls -->
              <div class="user-controls">
              <ul>
                 <li class="messages">
                     Bonjour {{ $user->T01_firstname_va }} <strong>{{ $user->T01_lastname_va }}</strong>
                    <div class="profile-photo">
                      {{ $user->T01_avatar_va }}
                    </div>
                  </li>
                  <li class="dropdown settings">
                    <a class="dropdown-toggle options" data-toggle="dropdown" href="#">
                      <i class="fa fa-cog"></i>
                    </a>
                    <ul class="dropdown-menu arrow">
                      <li>
                        <a href="{{ $base_url_backend }}profil"><i class="fa fa-user"></i> Profil</a>
                      </li>
                      <li class="divider">
                        
                      </li>
                      <li>
                        <a href="{{ $base_url_backend }}logout"><i class="fa fa-power-off"></i> Deconnexion</a>
                      </li>
                    </ul>
                  </li>

                </ul>
              </div>
              <!-- User Controls end -->

            



            </div>
            <!-- /Content collapsing at 768px to sidebar -->



            <!-- Sidebar -->
            <ul class="nav navbar-nav side-nav" id="navigation">
              <li class="collapsed-content">
                <!-- Collapsed content pasting here at 768px -->
              </li>
              <li>
                <a href="{{ $base_url_backend }}"><i class="fa fa-user"></i>Dashboard</a>
              </li>
              <li>
                <a href="{{ $base_url_backend }}liste_articles" title="Articles">
                  <i class="fa fa-tint">
                    <span class="overlay-label orange"></span>
                  </i>
                  Articles
                </a>
              </li>
              <li>
                <a href="{{ $base_url_backend }}liste_commentaires" title="Commentaires">
                  <i class="fa fa-tint">
                    <span class="overlay-label orange"></span>
                  </i>
                  Commentaires
                </a>
              </li>
              <!--
              <li>
                <a href="{{ $base_url_backend }}pages" title="Pages">
                  <i class="fa fa-tint">
                    <span class="overlay-label orange"></span>
                  </i>
                  Pages
                </a>
              </li>
              <li>
                <a href="{{ $base_url_backend }}categories" title="Catégories">
                  <i class="fa fa-th">
                    <span class="overlay-label cyan"></span> 
                  </i>
                  Catégories
                </a>
              </li>
              <li>
                <a href="{{ $base_url_backend }}mediatheque" title="Médiathèque">
                  <i class="fa fa-th">
                    <span class="overlay-label cyan"></span> 
                  </i>
                  Médias
                </a>
              </li>
               -->
              <li>
                <a href="{{ $base_url_backend }}parametres" title="Paramètres">
                  <i class="fa fa-gear">
                    <span class="overlay-label cyan"></span> 
                  </i>
                  Paramètres
                </a>
              </li>

            </ul>
            <!-- Sidebar end -->
          </div>
          <!--/.nav-collapse -->
        </div>
        <!-- Fixed navbar end -->
        <!-- Page content -->
        <div id="content" class="col-md-12">
          
          <!-- submenu -->
         
            <h2>@yield('title')</h2>
            
            
         
          <!-- /submenu -->
          <!-- content main container -->
          <div class="main" >

             {!! $flash !!}
             
            @yield('content')

          </div>
          <!-- /content container -->
        </div>
        <!-- Page content end -->
      </div>
      <!-- Make page fluid-->
    </div>
    <!-- Wrap all page content end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ $base_url }}vendor/components/jquery/jquery.js"></script>
    <script src="{{ $base_url }}vendor/components/jqueryui/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ $assets_url }}js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=sons-of-obsidian"></script>
    <script src="{{ $assets_url }}js/plugins/jquery.nicescroll.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/blockui/jquery.blockUI.js"></script>
    <script src="{{ $assets_url }}js/plugins/rickshaw/raphael-min.js"></script> 
    <script src="{{ $assets_url }}js/plugins/rickshaw/d3.v2.js"></script>
    <script src="{{ $assets_url }}js/plugins/rickshaw/rickshaw.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/skycons/skycons.js"></script>
    <script src="{{ $assets_url }}js/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/tabdrop/bootstrap-tabdrop.min.js"></script>
    <script src="{{ $assets_url }}js/plugins/morris/morris.min.js"></script>
    <script src="{{ $assets_url }}js/minoral.js"></script>
    <script src="{{ $base_url }}vendor/tinymce/tinymce/tinymce.min.js"></script>
    <script src="{{ $base_url }}vendor/enyo/dropzone/dist/min/dropzone.min.js"></script>
    
    <script>
        (function( $ ){
          @yield('scripts')
        })(jQuery);
    </script>
  </body>
</html>