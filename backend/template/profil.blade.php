@extends('master')

@section('title')
  <h2>Profil</h2>
  <p></p>
@endsection

@section('content')
    <section class="tile cornered">
    	<div class="tile-header">
	        <div class="controls">
	          <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
	          <a href="#" class="remove"><i class="fa fa-times"></i></a>
	        </div>
	      </div>
	    <div class="tile-body">
	    	<div class="row">
				<div class="col-lg-9">

				    <div class="panel panel-primary">
				      <div class="panel-heading">
				        <h3 class="panel-title">Identification</h3>
				        
				        </form>
				        
				      </div>
				      <div class="panel-body">
				       	<form method="POST" action="{{ $base_url_backend }}profil" class="form-horizontal">
				       		<input type="hidden" name="hash" value="{{ $hash }}" />
				       		<input type="hidden" name="T01_codeinterne_i" value="{{ $user->T01_codeinterne_i }}" />
					        <div class="form-group">
						        <label for="T01_login_va" class="col-sm-4 control-label">Identifiant</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="T01_login_va" name="T01_login_va" value="{{ $user->T01_login_va }}" />
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="T01_email_va" class="col-sm-4 control-label">Email</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="T01_email_va" name="T01_email_va" value="{{ $user->T01_email_va }}" />
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="T01_password_va" class="col-sm-4 control-label">Mot de passe</label>
						        <div class="col-sm-8">
						        	<input type="password" class="form-control" id="T01_password_va" name="T01_password_va" value="" />
						        	<small>Laisser vide pour ne pas changer le mot de passe</small>
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="password_verif" class="col-sm-4 control-label">Répetez le mot de passe</label>
						        <div class="col-sm-8">
						        	<input type="password" class="form-control" id="password_verif" name="password_verif" value="" />	
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="T01_lastname_va" class="col-sm-4 control-label">Nom</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="T01_lastname_va" name="T01_lastname_va" value="{{ $user->T01_lastname_va }}" />
						       	</div>
					        </div>

					        <div class="form-group">
						        <label for="T01_firstname_va" class="col-sm-4 control-label">Prénom</label>
						        <div class="col-sm-8">
						        	<input type="text" class="form-control" id="T01_firstname_va" name="T01_firstname_va" value="{{ $user->T01_firstname_va }}" />
						       	</div>
					        </div>

				        	<p class="text-right"><input type="submit" value="sauvegarder" class="btn btn-primary" /></p>
				      </div>
				    </div>
			  	</div>

	
			</div>
			
		</div>

   </section>

  </div>

@endsection

@section('script')
 
@endsection