<?php
  
  require '../vendor/autoload.php';
  require "../includes/config.php";
  require "../includes/init.php";

  use Philo\Blade\Blade;
  use App\Config;
  use App\Debug;
  use App\UI;
  use App\Router;
  use App\Myerrorhandler;
  use App\Controllers\BackendController;
  use App\Models\UserManager;

  session_start();

  Myerrorhandler::getInstance();

  $view_params = false;

  /* On défini les url de base des vues */
  $utils->setBaseUrl($config->base_url_backend);
  $utils->setAssetsUrl($config->assets_url_backend);

  /* on instancie le controleur de l'application */
  $app = new BackendController($config->conn);
  $app->setConfig($config);


  /* initialisation moteur de template Blade */
  $views =  $app->config()->path . '/backend/template';
  $cache =  $app->config()->path . '/cache';
  
  $blade = new Blade($views, $cache);

  if(isset($_SESSION['user'])){$app->setLogged(true);}
  if(isset($_SESSION['user'])){$app->setUser($_SESSION['user']);}
 
  /* On initialise le router */
  $router = new Router($config->conn, $blade);
  
  /* routage */
  $query =  $_SERVER['QUERY_STRING'];
  $request =  $_SERVER['REQUEST_URI'];
  $method =  $_SERVER['REQUEST_METHOD'];
  $router->add($query, $request, $method);

  /* On protège l'accès au backend */
  if(!$app->logged())
  {
    /* - Chercher si le paramètre 'url' correspond à une route définie dans le controleur Backend */
    $view_params = $router->call($app, ['identification']);

    if(!$view_params)
    {
       /* On protège l'accès au backend */   
      $view_params = [
        "view" => "login"
      ];
    }
  }
  else
  {
    /*
      - Chercher si le paramètre 'url' correspond à un controleur existant
      - Chercher si le paramètre 'url' correspond à une vue existante 
    */

    $view_params = $router->run($app, 'dashboard');
  }

  if(!$view_params)
  {
    /* - Chercher si le paramètre 'url' correspond à une route définie dans le controleur Backend */
    $view_params = $router->call($app, ['logout'], 'dashboard');
  }
  
  /* On récupère les variables de vue si elles existent */
  $bladeDatas = $view_params['datas'];;

  if(!$view_params)
  {
    /*
      - Si auccun des cas précédent n'est trouvé renvoyer une erreur 404
    */
    header('HTTP/1.0 404 Not Found');
    $view_params = [
      "view" => "404"
    ];
  }
  
   /* Messages flash */
  $messages_flash =  $view_params['flash'];
  $flash = "";

  foreach ($messages_flash as $mFlash)
  {
    $flash .= UI::flash($mFlash['type'], $mFlash['title'], $mFlash['message'] );
  }

  $bladeDatas["flash"] =  $flash;

  /* Redirection */
  if(isset($view_params['redirect']))
  {
      $utils->redirect($view_params['redirect']);
      exit;
  }

  /* Identité de l'utilisateur identifié */
  if($app->logged())
  {
    $bladeDatas["user"] = $_SESSION['user'];
  }

  /* variables globales de la vue */
  $bladeDatas["base_url"] = $config->base_url;
  $bladeDatas["base_url_backend"] = $config->base_url_backend;
  $bladeDatas["assets_url"] = $config->assets_url_backend;

  /* on affiche la vue  */
  if ($blade->view()->exists($view_params['view']) )
  {
    $view = $view_params['view'];
  }
  else
  {
    $view = "404";
  }
  
  echo $blade->view()->make($view)->with($bladeDatas)->render();