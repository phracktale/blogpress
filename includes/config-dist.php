<?php
    
    $path = __DIR__ ;
    $iLen = strlen('includes');
    $path = substr($path, 0, strlen($path) - $iLen);
    $config_vars = [
    	'base_url' 				=>	'http://host',
    	'assets_url' 			=>	'http://host/public/assets/',
    	'base_url_backend'		=>	'http://host/backend/',
    	'assets_url_backend'	=>	'http://host/assets/',
    	'db_host'				=>	'localhost',
    	'db_name'				=>	'blogpress',
    	'db_user'				=>	'blogpress',
    	'db_password'           =>  'PASSWORD',
        'sender'                =>  [ 
                                         'name' => 'administrateur du blog', 
                                         'email' => 'admin@blogpress.com' 
                                    ],
        'path'                  =>  $path,
        'pagination'            => 2
    ];

?>