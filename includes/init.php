<?php
	/* Instantiation des classes */
	$utils = new \App\Utils();
	$config = new \App\Config($config_vars);

	/* Initialisation de la base de données */
	if(!$config->init_db())
	{
		$messages_flash[]  =  [
			'type' 		=> 'alert-warning',
			'title' 	=> "Cette application n'est pas installée"
		];
	}

	/* Vérifier les répertoires et créer les manquants */
